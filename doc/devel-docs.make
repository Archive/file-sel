EXTRA_DIST += $(app).sgml

all: index.html

index.html: $(app)/index.html
	-cp $(app)/index.html .

# the wierd srcdir trick is because the db2html from the Cygnus RPMs
# cannot handle relative filenames
$(app)/index.html: $(srcdir)/$(app).sgml $(extra_deps)
	-srcdir=`cd $(srcdir) && pwd`; \
	db2html $$srcdir/$(app).sgml

devel-dist-hook: index.html
	-$(mkinstalldirs) $(distdir)/$(app)/stylesheet-images
	-$(mkinstalldirs) $(distdir)/figures
	-cp $(srcdir)/$(app)/*.html $(distdir)/$(app)
	-cp $(srcdir)/$(app)/*.css $(distdir)/$(app)
	-cp $(srcdir)/$(app)/stylesheet-images/*.png \
		$(distdir)/$(app)/stylesheet-images
	-cp $(srcdir)/figures/*.png \
		$(distdir)/figures
