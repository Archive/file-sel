/*
 * flist-mime-icon.h: utility functions for getting icons for mime
 * types
 *
 * Authors:
 *    Jacob Berkman   <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_MIME_ICON_H_
#define _FLIST_MIME_ICON_H_

#include <libgnomevfs/gnome-vfs-types.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

char      *flist_mime_icon_get_file (const char *file_path, GnomeVFSFileType file_type, const char *mime_type);
GdkPixbuf *flist_mime_icon_load     (const char *file_path, GnomeVFSFileType file_type, const char *mime_type);

#endif /* _FLIST_MIME_ICON_H_ */
