/*
 * flist-mime-icon.c: utility functions for getting icons for mime
 * types
 *
 * Authors:
 *    Jacob Berkman   <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-mime-icon.h"

#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#define ICON_NAME_BLOCK_DEVICE          "i-blockdev.png"
#define ICON_NAME_BROKEN_SYMBOLIC_LINK  "i-symlink.png"
#define ICON_NAME_CHARACTER_DEVICE      "i-chardev.png"
#define ICON_NAME_DIRECTORY             "i-directory.png"
#define ICON_NAME_EXECUTABLE            "i-executable.png"
#define ICON_NAME_FIFO                  "i-fifo.png"
#define ICON_NAME_INSTALL		"services-rpm.png"
#define ICON_NAME_REGULAR               "i-regular.png"
#define ICON_NAME_SEARCH_RESULTS        "i-search.png"
#define ICON_NAME_SOCKET                "i-sock.png"
#define ICON_NAME_THUMBNAIL_LOADING     "loading.png"
#define ICON_NAME_TRASH_EMPTY           "trash-empty.png"
#define ICON_NAME_TRASH_NOT_EMPTY       "trash-full.png"
#define ICON_NAME_WEB			"i-web.png"

#define ICON_NAME_DESKTOP               "my_desktop.png"
#define ICON_NAME_HOME                  "my_home.png"
#define ICON_NAME_ROOT                  "my_computer.png"
#define ICON_NAME_DOCUMENTS             "my_documents.png"

#define ICON_SIZE 18.0

static GHashTable *icon_hash = NULL;
static GHashTable *dir_hash = NULL;

static void
create_hashes (void)
{
	g_return_if_fail (icon_hash == NULL);
	g_return_if_fail (dir_hash == NULL);

	icon_hash = g_hash_table_new (g_str_hash, g_str_equal);
	dir_hash  = g_hash_table_new (g_str_hash, g_str_equal);

	/* FIXME: don't leak */
	/* (note: only one leaks) */
	g_hash_table_insert (dir_hash, g_get_home_dir (), 
			     ICONDIR"/"ICON_NAME_HOME);
	g_hash_table_insert (dir_hash, gnome_util_prepend_user_home (".gnome-desktop"),
			     ICONDIR"/"ICON_NAME_DESKTOP);
	g_hash_table_insert (dir_hash, gnome_util_prepend_user_home ("Documents"),
			     ICONDIR"/"ICON_NAME_DOCUMENTS);
	g_hash_table_insert (dir_hash, "/", ICONDIR"/"ICON_NAME_ROOT);
}

char *
flist_mime_icon_get_file (const char *file_path, GnomeVFSFileType file_type, const char *mime_type)
{
	const char *icon = NULL;
	char *file = NULL;

	if (!dir_hash)
		create_hashes ();

	switch (file_type) {
	case GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK:
		icon = ICON_NAME_BROKEN_SYMBOLIC_LINK;
	default:
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
	case GNOME_VFS_FILE_TYPE_REGULAR:
		if (mime_type) 
			icon = gnome_vfs_mime_get_icon (mime_type);
		if (!icon) 
			icon = ICON_NAME_REGULAR;			
		break;
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		file = g_hash_table_lookup (
			dir_hash,
			strstr (file_path, "file:///") == file_path
			? file_path + 7 
			: file_path);
		icon = ICON_NAME_DIRECTORY;
		break;
	case GNOME_VFS_FILE_TYPE_CHARACTER_DEVICE:
		icon = ICON_NAME_CHARACTER_DEVICE;
		break;
	case GNOME_VFS_FILE_TYPE_FIFO:
		icon = ICON_NAME_FIFO;
		break;
	case GNOME_VFS_FILE_TYPE_SOCKET:
		icon = ICON_NAME_SOCKET;
		break;
	case GNOME_VFS_FILE_TYPE_BLOCK_DEVICE:
		icon = ICON_NAME_BLOCK_DEVICE;
		break;
	}

	if (!file) {
		if (!icon) return NULL;			
		file = g_concat_dir_and_file (MIMEICONDIR, icon);
	} else {
		/* g_print ("got magic icon: %s\n", file); */
		file = g_strdup (file);
	}

	return file;
}

GdkPixbuf *
flist_mime_icon_load (const char *file_path, GnomeVFSFileType file_type, const char *mime_type)
{
	GdkPixbuf *pb, *pb2;
	char *file = NULL;

	file = flist_mime_icon_get_file (file_path, file_type, mime_type);
	if (!file)
		return NULL;

	if (!icon_hash)
		create_hashes ();

	pb = g_hash_table_lookup (icon_hash, file);
	if (pb) {
		g_free (file);
		gdk_pixbuf_ref (pb);
		return pb;
	}

	pb2 = gdk_pixbuf_new_from_file (file);
	if (pb2) {
		double ar = (double)gdk_pixbuf_get_width (pb2) / (double)gdk_pixbuf_get_height (pb2);
		/* g_print ("ar: %f, width: %d\n", ar, (int)(ICON_SIZE * ar)); */
		pb = gdk_pixbuf_scale_simple (pb2, ICON_SIZE * ar, ICON_SIZE, GDK_INTERP_HYPER);
		gdk_pixbuf_unref (pb2);
	} else {
		g_message ("Could not load: `%s', trying default", file);
		g_free (file);
		pb = g_hash_table_lookup (icon_hash, MIMEICONDIR"/"ICON_NAME_REGULAR);
		if (pb) gdk_pixbuf_ref (pb);
		return pb;
	}
	/* may as well... */
	gdk_pixbuf_ref (pb);
	g_hash_table_insert (icon_hash, file, pb);
	return pb;
}

