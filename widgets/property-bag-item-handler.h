/*
 * property-bag-item-handler.h: a moniker item handler which can set
 * properties on a given property bag
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef PROPERTY_BAG_ITEM_HANDLER_H
#define PROPERTY_BAG_ITEM_HANDLER_H

#include <bonobo/bonobo-item-handler.h>
#include <bonobo/bonobo-property-bag.h>

BonoboItemHandler *property_bag_item_handler_new (BonoboObject *object,
						  BonoboPropertyBag *pb);

#endif /* PROPERTY_BAG_ITEM_HANDLER_H */
