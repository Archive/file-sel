/*
 * property-bag-item-handler.c: a moniker item handler which can set
 * properties on a given property bag
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include <ctype.h>
#include <stdlib.h>

#include "property-bag-item-handler.h"

static Bonobo_Unknown
get_object (BonoboItemHandler *h, 
	    const char *item_name, 
	    gboolean only_if_exists,
	    gpointer data, 
	    CORBA_Environment *ev)
{
	BonoboPropertyBag *pb;
	GSList *options, *l;
	BonoboItemOption *o;
	BonoboArg *arg;

	pb = data;

	options = bonobo_item_option_parse (item_name);
	for (l = options; l; l = l->next) {
		BonoboArgType arg_type;
		o = l->data;

		arg_type = bonobo_property_bag_get_property_type (pb, o->key, ev);
		if (!arg_type)
			continue;

		arg = bonobo_arg_new (arg_type);
		if (!arg)
			continue;

		switch (arg_type->kind) {
		case CORBA_tk_boolean:
			BONOBO_ARG_SET_BOOLEAN (arg,
						tolower (o->value[0]) == 't' || 
						tolower (o->value[0]) == 'y' || 
						atoi (o->value)
						? CORBA_TRUE
						: CORBA_FALSE);
			break;

		case CORBA_tk_long:
			BONOBO_ARG_SET_LONG (arg, atol (o->value));
			break;

		case CORBA_tk_float:
			BONOBO_ARG_SET_FLOAT (arg, atof (o->value));
			break;

		case CORBA_tk_double:
			BONOBO_ARG_SET_DOUBLE (arg, atof (o->value));
			break;

		case CORBA_tk_string:
			BONOBO_ARG_SET_STRING (arg, o->value);
			break;

		default:
			bonobo_arg_release (arg);
			continue;
		}

		bonobo_property_bag_set_value (pb, o->key, arg, ev);
		bonobo_arg_release (arg);
	}

	return bonobo_object_dup_ref (
		BONOBO_OBJECT (h)->corba_objref, ev);
}

BonoboItemHandler *
property_bag_item_handler_new (BonoboObject *object,
			       BonoboPropertyBag *pb)
{
	BonoboItemHandler *handler;

	/* FIXME: ref pb here, and unref on destroy */
	handler = bonobo_item_handler_new (NULL, get_object, pb);
	bonobo_object_add_interface (BONOBO_OBJECT (object),
				     BONOBO_OBJECT (handler));
	return handler;

}
