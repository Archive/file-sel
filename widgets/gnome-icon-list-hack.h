/*
 * gnome-icon-list-hack.h: a hacked up icon list
 *
 * Authors:
 *    Federico Mena   <federico@nuclecu.unam.mx>
 *    Miguel de Icaza <miguel@nuclecu.unam.mx>
 *    Jacob Berkman   <jacob@ximian.com>
 *
 * Copyright (C) 1998 The Free Software Foundation
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _GNOME_ICON_LIST_HACK_H_
#define _GNOME_ICON_LIST_HACK_H_

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>
#include <gdk_imlib.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_ICON_LIST_HACK            (gnome_icon_list_hack_get_type ())
#define GNOME_ICON_LIST_HACK(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_ICON_LIST_HACK, GnomeIconListHack))
#define GNOME_ICON_LIST_HACK_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_ICON_LIST_HACK, GnomeIconListHackClass))
#define GNOME_IS_ICON_LIST_HACK(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_ICON_LIST_HACK))
#define GNOME_IS_ICON_LIST_HACK_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_ICON_LIST_HACK))

typedef enum {
	GNOME_ICON_LIST_HACK_ICONS,
	GNOME_ICON_LIST_HACK_TEXT_BELOW,
	GNOME_ICON_LIST_HACK_TEXT_RIGHT
} GnomeIconListHackMode;

/* This structure has been converted to use public and private parts.  To avoid
 * breaking binary compatibility, the slots for private fields have been
 * replaced with padding.  Please remove these fields when gnome-libs has
 * reached another major version and it is "fine" to break binary compatibility.
 */
typedef struct {
	GnomeCanvas canvas;

	/* Scroll adjustments */
	GtkAdjustment *adj;
	GtkAdjustment *hadj;

	/* Number of icons in the list */
	int icons;

	/* Private data */
	gpointer priv; /* was GList *icon_list_hack */

	/* A list of integers with the indices of the currently selected icons */
	GList *selection;
} GnomeIconListHack;

typedef struct {
	GnomeCanvasClass parent_class;

	void     (*select_icon)    (GnomeIconListHack *gil, gint num, GdkEvent *event);
	void     (*unselect_icon)  (GnomeIconListHack *gil, gint num, GdkEvent *event);
	gboolean (*text_changed)   (GnomeIconListHack *gil, gint num, const char *new_text);
	int      (*icon_editable)  (GnomeIconListHack *gil, gint num);
} GnomeIconListHackClass;

#define GNOME_ICON_LIST_HACK_IS_EDITABLE 1
#define GNOME_ICON_LIST_HACK_STATIC_TEXT 2

guint          gnome_icon_list_hack_get_type            (void);

GtkWidget     *gnome_icon_list_hack_new                 (guint         icon_width,
							 GtkAdjustment *adj,
							 int           flags);
GtkWidget     *gnome_icon_list_hack_new_flags           (guint         icon_width,
							 GtkAdjustment *adj,
							 int           flags);
void           gnome_icon_list_hack_construct           (GnomeIconListHack *gil,
							 guint icon_width,
							 GtkAdjustment *adj,
							 int flags);

void           gnome_icon_list_hack_set_hadjustment    (GnomeIconListHack *gil,
							GtkAdjustment *hadj);

void           gnome_icon_list_hack_set_vadjustment    (GnomeIconListHack *gil,
							GtkAdjustment *vadj);

/* To avoid excesive recomputes during insertion/deletion */
void           gnome_icon_list_hack_freeze              (GnomeIconListHack *gil);
void           gnome_icon_list_hack_thaw                (GnomeIconListHack *gil);


void           gnome_icon_list_hack_insert              (GnomeIconListHack *gil,
							 int pos, const char *icon_filename,
							 const char *text);


void           gnome_icon_list_hack_insert_imlib        (GnomeIconListHack *gil,
							 int pos, GdkImlibImage *im,
							 const char *text);

void           gnome_icon_list_hack_insert_item         (GnomeIconListHack *gil,
							 int pos, GnomeCanvasItem *item,
							 const char *text);						    

int            gnome_icon_list_hack_append              (GnomeIconListHack *gil,
							 const char *icon_filename,
							 const char *text);
int            gnome_icon_list_hack_append_imlib        (GnomeIconListHack *gil,
							 GdkImlibImage *im,
							 const char *text);
int            gnome_icon_list_hack_append_item         (GnomeIconListHack *gil,
							 GnomeCanvasItem *item,
							 const char *text);

void           gnome_icon_list_hack_clear               (GnomeIconListHack *gil);
void           gnome_icon_list_hack_remove              (GnomeIconListHack *gil, int pos);


/* Managing the selection */
void           gnome_icon_list_hack_set_selection_mode  (GnomeIconListHack *gil,
							 GtkSelectionMode mode);
void           gnome_icon_list_hack_select_icon         (GnomeIconListHack *gil,
							 int idx);
void           gnome_icon_list_hack_unselect_icon       (GnomeIconListHack *gil,
							 int pos);
int            gnome_icon_list_hack_unselect_all        (GnomeIconListHack *gil,
							 GdkEvent *event, gpointer keep);

/* Setting the spacing values */
void           gnome_icon_list_hack_set_icon_width      (GnomeIconListHack *gil,
							 int w);
void           gnome_icon_list_hack_set_row_spacing     (GnomeIconListHack *gil,
							 int pixels);
void           gnome_icon_list_hack_set_col_spacing     (GnomeIconListHack *gil,
							 int pixels);
void           gnome_icon_list_hack_set_text_spacing    (GnomeIconListHack *gil,
							 int pixels);
void           gnome_icon_list_hack_set_icon_border     (GnomeIconListHack *gil,
							 int pixels);
void           gnome_icon_list_hack_set_separators      (GnomeIconListHack *gil,
							 const char *sep);

/* Attaching information to the icons */
void           gnome_icon_list_hack_set_icon_data       (GnomeIconListHack *gil,
							 int pos, gpointer data);
void           gnome_icon_list_hack_set_icon_data_full  (GnomeIconListHack *gil,
							 int pos, gpointer data,
							 GtkDestroyNotify destroy);
int            gnome_icon_list_hack_find_icon_from_data (GnomeIconListHack *gil,
							 gpointer data);
gpointer       gnome_icon_list_hack_get_icon_data       (GnomeIconListHack *gil,
							 int pos);

/* Visibility */
void           gnome_icon_list_hack_moveto              (GnomeIconListHack *gil,
							 int pos, double yalign);
GtkVisibility  gnome_icon_list_hack_icon_is_visible     (GnomeIconListHack *gil,
							 int pos);

int            gnome_icon_list_hack_get_icon_at         (GnomeIconListHack *gil, int x, int y);

int            gnome_icon_list_hack_get_items_per_line  (GnomeIconListHack *gil);

END_GNOME_DECLS

#endif /* _GNOME_ICON_LIST_HACK_H_ */
