/*
 * file-selector-factory.c: factory for file selector components
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001, Ximian, Inc.
 */

#include <config.h>

#include "flist/flist.h"
#include "flist/flist-entry.h"
#include "fsel/file-selector.h"

#include <bonobo/bonobo-shlib-factory.h>

#include <libgnomevfs/gnome-vfs-init.h>

#define OID_MATCH(o, s) (!strcmp (o, "OAFIID:GNOME_FileSelector_" s))

static BonoboObject *
fsel_factory (BonoboGenericFactory *factory,
	      const char           *object_id,
	      gpointer              data)
{
	BonoboObject *o = NULL;

	if (!gnome_vfs_initialized () &&
	    !gnome_vfs_init ()) {
		g_warning (_("Could not initialize gnome-vfs"));
		return NULL;
	}

	if (OID_MATCH (object_id, "Control")) {
		o = file_selector_control_new ();
	} else if (OID_MATCH (object_id, "List")) {
		o = BONOBO_OBJECT (gtk_type_new (FLIST_TYPE));
	} else if (OID_MATCH (object_id, "Entry")) {
		o = file_selector_entry_new ();
	}
	
	return o;
}

BONOBO_OAF_SHLIB_FACTORY_MULTI ("OAFIID:GNOME_FileSelector_Factory",
				"Factory for FileSelector components",
				fsel_factory, NULL);
