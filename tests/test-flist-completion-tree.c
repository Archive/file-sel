/*
 * flist-completion-tree-test.c: a test of flist-completion-tree
 *
 * adapted from gal/gal/e-text/e-completion-test.c
 *
 * Authors:
 *    Jacob Berkman
 *    Jon Trowbridge
 *
 * Copyright 2001 Ximian, Inc. 
 */

#include <config.h>

#include "flist/flist-completion-tree.h"

#include <gal/e-text/e-entry.h>

#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkwindow.h>

#include <libgnomeui/gnome-init.h>

#define TIMEOUT 10

/* Dictionary Lookup test */
static GNode *dictionary;
static int dict_size;

static void
read_dict (void)
{
	FILE *in = fopen ("/usr/share/dict/words", "r");
	gchar buffer[BUFSIZ];
	gint len;
	gpointer p;

	dict_size = 0;

	while (fgets (buffer, BUFSIZ, in)) {
		len = strlen (buffer);
		if (len > 0 && buffer[len-1] == '\n')
			buffer[len-1] = '\0';
		p = flist_completion_tree_insert (dictionary, buffer, g_strdup (buffer));
		/*g_print ("inserted: %p\t%s\n", p, buffer);*/
		dict_size++;
	}
	fclose (in);
}

struct {
	ECompletion *complete;
	const gchar *txt;
	gint start;
	gint current;
	gint len;
	gint limit;
	gint count;
} dict_info;
static guint dict_tag = 0;

int len;

static gboolean
visit (GNode *node, gpointer data)
{
	ECompletionMatch *m;
	const char *s;
	
	s = flist_completion_tree_data (node);

	if (!s)
		return FALSE;

	/*g_print ("\t%p %s\n", node, s);*/
	
	m = e_completion_match_new (s, s, strlen (s) / (double)len);
	e_completion_found_match (data, m);
	return FALSE;
}

static void
begin_dict_search (ECompletion *complete, const gchar *txt, gint pos, gint limit, gpointer user_data)
{
	GNode *match;

	len = strlen (txt);

	g_print ("begin\n");

	match = flist_completion_tree_lookup (dictionary, txt);
	
	if (match)
		g_node_traverse (match, G_IN_ORDER, G_TRAVERSE_ALL, -1, visit, complete);
	
	g_print ("end\n");

	e_completion_end_search (complete);
}

static gboolean
print_it (GNode *node, gpointer data)
{
	char *s;

	s = flist_completion_tree_data (node);

	g_print ("%s\n", s);

	return FALSE;
}

int
main (int argc, gchar **argv)
{
	ECompletion* complete;
	GtkWidget *entry;
	GtkWidget *win;
	GNode *node;
	gpointer data;

	dictionary = g_node_new (NULL);
#if 0
	flist_completion_tree_insert (dictionary, "test", "test");
	flist_completion_tree_insert (dictionary, "text", "text");

	node = flist_completion_tree_lookup (dictionary, "te");

	if (node)
		g_node_traverse (node, G_IN_ORDER, G_TRAVERSE_ALL, -1, print_it, NULL);
#else
	read_dict ();

	gnome_init ("FListCompletionTest", "0.0", argc, argv);	

	complete = e_completion_new ();
	gtk_signal_connect_after (GTK_OBJECT (complete),
				  "begin_completion",
				  GTK_SIGNAL_FUNC (begin_dict_search),
				  complete);

	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	entry = e_entry_new ();
	e_entry_enable_completion_full (E_ENTRY (entry), complete, 0, NULL);
	e_entry_set_editable (E_ENTRY (entry), TRUE);

	gtk_container_add (GTK_CONTAINER (win), entry);
	gtk_widget_show_all (win);

	gtk_main ();
#endif
	return 0;
}
