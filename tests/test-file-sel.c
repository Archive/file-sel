/*
 * test-file-sel.c: test the file selector
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *    Lutz M�ller  <urc8@rz.uni-karlsruhe.de>
 *
 * Copyright 2001 Ximian, Inc.
 *          
 */

#include <config.h>

#include <liboaf/oaf-activate.h>
#include <liboaf/oaf-mainloop.h>

#include <gtk/gtkmain.h>
#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkwindow.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-init.h>

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-widget.h>

static void
cb_delete_event (GtkWidget *widget, GdkEventAny *event, gpointer data)
{
	g_message ("Got delete_event!");

	gtk_main_quit ();
}

static void
listener_cb (BonoboListener *listener, 
	     gchar *event_name,
	     CORBA_any *any,
	     CORBA_Environment *ev,
	     gpointer data)
{
	CORBA_sequence_CORBA_string *seq;
	int i;

	g_message ("Got listener callback for '%s'!", event_name);

	if (!strcmp (event_name, "GNOME/FileSelector/Control:ButtonClicked:Action")) {
		
		seq = any->_value;

		g_message ("Now we would open: \t");
		for (i = 0; i < seq->_length; i++) {
			g_print ("\t%s\n", seq->_buffer[i]);
		}

		gtk_main_quit ();
	} else if (!strcmp (event_name, "GNOME/FileSelector/Control:ButtonClicked:Cancel")) {
		
		g_message ("Operation cancelled!");
		
		gtk_main_quit ();
	}
}

typedef struct {
	Bonobo_EventSource  event_source;
	BonoboListener     *listener;
	Bonobo_EventSource_ListenerId id;
} MyData;

static gboolean
create_listener (gpointer data)
{
	MyData *my_data = data;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	g_message ("Adding listener...");
	my_data->id = 
		Bonobo_EventSource_addListener (my_data->event_source,
						BONOBO_OBJREF (my_data->listener), &ev);

	if (BONOBO_EX (&ev))
		g_error (bonobo_exception_get_text (&ev));

	CORBA_exception_free (&ev);

	return (FALSE);
}

static const char *moniker_args = NULL;

static const struct poptOption options[] = {
	{ "args", 0, POPT_ARG_STRING, &moniker_args, 0, N_("Arguments for moniker"), N_("ARGS") },
	{ NULL }
};

int
main (int argc, char *argv[])
{
	CORBA_ORB orb;
	CORBA_Environment ev;
	Bonobo_Control control;
	GtkWidget *window, *widget;
	BonoboUIContainer *container;
	BonoboUIEngine *engine;
	MyData *my_data;
	char *moniker;

	gnome_init_with_popt_table ("test-file-sel", VERSION,
				    argc, argv, options, 0, NULL);

	orb = oaf_init (argc, argv);
	if (!bonobo_init (orb, NULL, NULL))
		g_error ("Can not initialize bonobo!");

	CORBA_exception_init (&ev); 
	
	if (moniker_args && strlen (moniker_args))
		moniker = g_strconcat ("OAFIID:GNOME_FileSelector_Control!", moniker_args, NULL);
	else
		moniker = "OAFIID:GNOME_FileSelector_Control";

	g_message ("Activating %s...", moniker);

	control = bonobo_get_object (moniker, "IDL:Bonobo/Control:1.0", &ev);

	if (moniker_args && strlen (moniker_args))
		g_free (moniker);

	if (BONOBO_EX (&ev))
		g_error (bonobo_exception_get_text (&ev));

	my_data = g_new0 (MyData, 1);
	g_message ("Querying EventSource...");
	my_data->event_source = 
		Bonobo_Unknown_queryInterface (control,
					       "IDL:Bonobo/EventSource:1.0", 
					       &ev);

	if (BONOBO_EX (&ev))
		g_error (bonobo_exception_get_text (&ev));

	g_message ("Creating window...");
	window = bonobo_window_new (argv[0], argv[0]);
	gtk_widget_show (window); 

	engine = bonobo_window_get_ui_engine (BONOBO_WINDOW (window));

	container = bonobo_ui_container_new ();
	bonobo_ui_engine_set_ui_container (engine, BONOBO_OBJECT (container));

	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
			    GTK_SIGNAL_FUNC (cb_delete_event), NULL);

	g_message ("Getting widget...");
	widget = bonobo_widget_new_control_from_objref (control,
							BONOBO_OBJREF (container));
	gtk_widget_show (widget);

	g_message ("Setting contents...");
	bonobo_window_set_contents (BONOBO_WINDOW (window), widget);

	g_message ("Creating listener...");
	my_data->listener = bonobo_listener_new (listener_cb, window);

	g_message ("Scheduling listener addition...");
	gtk_idle_add (create_listener, my_data);

	bonobo_main ();

	g_message ("Removing Listener...");
	
	if (my_data->id)
		Bonobo_EventSource_removeListener (my_data->event_source,
						   my_data->id,
						   &ev);
	if (BONOBO_EX (&ev))
		g_error (bonobo_exception_get_text (&ev));
	
	bonobo_object_release_unref (my_data->event_source, &ev);

	if (BONOBO_EX (&ev))
		g_error (bonobo_exception_get_text (&ev));

	gtk_widget_destroy (window);

	bonobo_object_unref (BONOBO_OBJECT (my_data->listener));
	bonobo_object_unref (BONOBO_OBJECT (container));

	CORBA_exception_free (&ev);

	return (0);
}
