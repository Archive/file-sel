#!/usr/bin/env python
import bonobo
import gtk
import CORBA
import time

# IDL
import Bonobo

def listener_cb (listener, event_name, any):
    subtype = bonobo.event_subtype (event_name)
    
    if subtype == "Action":
	print "You clicked on OK and the following files are selected"
	for item in any.value:
	    print item
    elif subtype == "Cancel":
	gtk.mainquit()
    
def idle ():
    # Create a GtkWindow
    win = gtk.GtkWindow ()
    win.connect ('delete_event', gtk.mainquit)
    
    # Create the fileselector
    control = bonobo.BonoboWidget ("OAFIID:GNOME_FileSelector")
    corba_control = control.get_objref ()
    
    # Query for an event source
    es = corba_control.queryInterface ("IDL:Bonobo/EventSource:1.0")
    
    # Create a new listener
    listener = bonobo.BonoboListener (listener_cb)
    corba_listener = listener.corba_objref ()
    es.addListenerWithMask (corba_listener, "GNOME/FileSelector:ButtonClicked")
    
    # Add the control to the window and show it
    win.add (control)
    win.show_all ()

bonobo.init()
gtk.idle_add (idle)
bonobo.main()
