/*
 * flist-util.c: some unix util functions
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

/* 
 * Based on some of mc/src/utilunix.c
 *
 * Authors:
 *    Miguel de Icaza
 *    Janne Kukonlehto
 *    Dugan Porter,
 *    Jakub Jelinek
 *    Mauricio Plaza
 *
 * Copyright (C) 1994, 1995, 1996 the Free Software Foundation.
 */

#include <config.h>

#include "flist-util.h"

#include <pwd.h>
#include <grp.h>
#include <sys/types.h>

#include <glib.h>

/* #define USE_HASH */

const char *
flist_util_get_username (int uid)
{
	struct passwd *pwd;
	static char ibuf [10];
    
#ifdef USE_HASH
	char *name;
	static GHashTable *uid_hash = NULL;
	if (!uid_hash)
		uid_hash = g_hash_table_new (g_int_hash, g_int_equal);

	if (name = g_hash_table_lookup (uid_hash, GINT_TO_POINTER (uid)))
		return name;
#endif
    
	pwd = getpwuid (uid);
	if (pwd){
#ifdef USE_HASH
		g_hash_table_add (uid_hash, GINT_TO_POINTER (uid), pwd->pw_name);
#endif
		return pwd->pw_name;
	}
	else {
		g_snprintf (ibuf, sizeof (ibuf), "%d", uid);
		return ibuf;
	}
}

const char *
flist_util_get_groupname (int gid)
{
	struct group *grp;
	static char gbuf [10];

#ifdef USE_HASH
	char *name;
	static GHashTable *gid_hash = NULL;
    
	if (!gid_hash)
		gid_hash = g_hash_table_new (g_int_hash, g_int_equal);

	if ((name = g_hash_table_lookup (gid_hash, GINT_TO_POINTER (gid)))
		return name;
#endif
	grp = getgrgid (gid);
	if (grp){
#ifdef USE_HASH
		g_hash_table_insert (gid_hash, GINT_TO_POINTER (gid), grp->gr_name);
#endif
		return grp->gr_name;
	} else {
		g_snprintf (gbuf, sizeof (gbuf), "%d", gid);
		return gbuf;
	}
}
