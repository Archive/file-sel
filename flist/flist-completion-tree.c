/*
 * flist-completion-tree.c: a tree for storing possible completions
 *
 * Authors:
 *    Jacob Berkman
 *
 * Copyright 2001 Ximian, Inc.
 */

/*
 * I guess my (partial) college education was worth it after all.
 *
 * -jcb
 */

/* FIXME: don't always fully expand new entries */

#include "flist-completion-tree.h"

#define FCDATA(n) ((FCData *)(n)->data)

typedef struct {
	gpointer data;
	char c;
} FCData;

/* debug insert, debug lookup */
#define di(x)
#define dl(x)

static gpointer
fc_insert (GNode *node, const char *s, gpointer data)
{
	FCData *fcdata;
	GNode *child;

	if (!*s) {
		gpointer retval;
		retval = FCDATA (node)->data;
		FCDATA (node)->data = data;
		return retval;
	}

	for (child = node->children; child; child = g_node_next_sibling (child))
		if (FCDATA (child)->c == *s)
			return fc_insert (child, s+1, data);

	fcdata = g_new (FCData, 1);
	fcdata->data = NULL;
	fcdata->c    = *s;

	child = g_node_new (fcdata);
	g_node_prepend (node, child);

	/* FIXME: inline this part */
	return fc_insert (child, s+1, data);
}

gpointer
flist_completion_tree_insert (GNode *root, const char *s, gpointer data)
{
	g_return_val_if_fail (root != NULL, NULL);
	g_return_val_if_fail (s != NULL, NULL);

	di(g_print ("inserting: %s\n", s));

	return fc_insert (root, s, data);
}

static GNode *
fc_lookup (GNode *node, const char *s)
{
	GNode *child;

	if (!*s)
		return node;

	for (child = node->children; child; child = g_node_next_sibling (child))
		if (FCDATA (child)->c == *s)
			return fc_lookup (child, s+1);

	return NULL;
}

GNode *
flist_completion_tree_lookup (GNode *root, const char *s)
{
	g_return_val_if_fail (root != NULL, NULL);
	g_return_val_if_fail (s != NULL, NULL);

	dl(g_print ("looking up: %s\n", s));

	return fc_lookup (root, s);
}

gpointer
flist_completion_tree_data (GNode *node)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (node->data != NULL, NULL);

	return FCDATA (node)->data;
}

typedef struct {
	GFunc    free_func;
	gpointer data;
} FreeClosure;

static gboolean
free_node (GNode *node, gpointer data)
{
	FreeClosure *closure;
	FCData      *fcdata;

	fcdata  = FCDATA (node);
	closure = data;
       
	/* we are the root */
	if (!fcdata)
		return FALSE;

	if (fcdata->data && closure->free_func)
		closure->free_func (fcdata->data, closure->data);

	g_free (fcdata);
	return FALSE;
}

GNode *
flist_completion_tree_new (void)
{
	return g_node_new (NULL);
}

void
flist_completion_tree_free (GNode *root, GFunc free_func, gpointer data)
{
	FreeClosure closure;

	g_return_if_fail (root != NULL);
	g_return_if_fail (root->data == NULL);

	closure.free_func = free_func;
	closure.data      = data;

	g_node_traverse (root, G_IN_ORDER, G_TRAVERSE_ALL, -1, free_node, &closure);
	g_node_destroy (root);
}
