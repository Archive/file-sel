/*
 * flist-table.c: table view for the flist
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-table.h"
#include "flist-dir.h"

/* bug in gnome-util.h */
#include <libgnome/gnome-defs.h>

#include <libgnome/gnome-util.h>
#include <gal/e-table/e-table-simple.h>
#include <gal/e-table/e-table-scrolled.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#define FLIST_TABLE_KEY "FListTable"

typedef struct {
	GtkWidget   *widget;
	FList       *flist;
	FListDir    *dir;
	ETableModel *etm;
	ETable      *table;

	guint        uri_changed_id;
	guint        uri_requested_id;
	guint        load_cancelled_id;
	guint        mime_types_changed_id;
	guint        multi_select_changed_id;
	guint        enable_vfs_changed_id;
} FListTable;

enum {
	COL_ICON, /* 0 */
	COL_NAME,
	COL_SIZE,
	COL_MIME,
	COL_MTIME,
	COL_ATIME, /* 5 */
	COL_CTIME,
	COL_USER,
	COL_GROUP,
	COL_LAST
};

const char *list_spec = "\
<ETableSpecification cursor-mode=\"line\" selection_mode=\"single\"> \
  <ETableColumn  model_col=\"0\"  _title=\"Icon\"      expansion=\"1.0\"  minimum_width=\"24\"  resizable=\"false\" cell=\"pixbuf\"  compare=\"integer\" sortable=\"false\"/> \
  <ETableColumn  model_col=\"1\"  _title=\"Name\"      expansion=\"1.0\"  minimum_width=\"40\"  resizable=\"true\"  cell=\"string\"  compare=\"string\"   /> \
  <ETableColumn  model_col=\"2\"  _title=\"Size\"      expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"size\"    compare=\"integer\"  /> \
  <ETableColumn  model_col=\"3\"  _title=\"Type\"      expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"string\"  compare=\"string\"   /> \
  <ETableColumn  model_col=\"4\"  _title=\"Modified\"  expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"date\"    compare=\"integer\"  /> \
  <ETableColumn  model_col=\"5\"  _title=\"Accessed\"  expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"date\"    compare=\"integer\"  /> \
  <ETableColumn  model_col=\"6\"  _title=\"Created\"   expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"date\"    compare=\"integer\"  /> \
  <ETableColumn  model_col=\"7\"  _title=\"Owner\"     expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"string\"  compare=\"string\"   /> \
  <ETableColumn  model_col=\"8\"  _title=\"Group\"     expansion=\"1.0\"  minimum_width=\"10\"  resizable=\"true\"  cell=\"string\"  compare=\"string\"   /> \
</ETableSpecification>";

const char *list_state = "\
<ETableState> \
  <column source=\"0\"/> \
  <column source=\"1\"/> \
  <column source=\"2\"/> \
  <column source=\"3\"/> \
  <column source=\"4\"/> \
  <grouping> \
    <leaf column=\"1\" ascending=\"true\"/> \
  </grouping> \
</ETableState>";

static int
fm_column_count (ETableModel *etm, gpointer data)
{
	return COL_LAST;
}

static int
fm_row_count (ETableModel *etm, gpointer data)
{
	return flist_dir_get_size (((FListTable *)data)->dir);
}

static void
fm_append_row (ETableModel *etm, ETableModel *model, int row, void *data)
{

}

static const void *
fm_value_at (ETableModel *etm, int col, int row, gpointer data)
{
	FListDir *fd = ((FListTable *)data)->dir;
	const GnomeVFSFileInfo *info;

	info = flist_dir_get_vfs_info (fd, row);

	switch (col) {
	case COL_ICON:
		return flist_dir_get_pixbuf (fd, row);
	case COL_NAME:
		return info->name;
	case COL_SIZE:
		return GINT_TO_POINTER (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_SIZE
					? (int)info->size : 0);
	case COL_MTIME:
		return GINT_TO_POINTER (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_MTIME
					? info->mtime : 0);
	case COL_MIME:
		return flist_dir_get_description (fd, row);
	case COL_ATIME:
		return GINT_TO_POINTER (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_ATIME
					? info->atime : 0);
	case COL_CTIME:
		return GINT_TO_POINTER (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_CTIME
					? info->ctime : 0);
	case COL_USER:
		return flist_dir_get_username (fd, row);
	case COL_GROUP:
		return flist_dir_get_groupname (fd, row);
	default:		
		g_warning ("unimplemented value_at(): %d", col);
		return NULL;
	}
}

static void
fm_set_value_at (ETableModel *etm, int col, int row, const void *value, FListTable *table)
{
	flist_dir_rename (table->dir, row, value);
}

static gboolean
fm_is_cell_editable (ETableModel *etm, int col, int row, gpointer data)
{
	switch (col) {
	case COL_NAME:
		return TRUE;
	default:
		return FALSE;
	}
}

static gboolean
fm_has_save_id (ETableModel *etm, void *data)
{
    return(FALSE);
}

static char *
fm_get_save_id (ETableModel *etm, int row, void *data)
{
    return(NULL);
}

static void *
fm_duplicate_value (ETableModel *etm, int col, const void *value, gpointer data)
{
	switch (col) {
	case COL_ICON:
		return value ? gdk_pixbuf_copy (value) : NULL;
	case COL_ATIME:
	case COL_CTIME:
	case COL_MTIME:
	case COL_SIZE:
		return GINT_TO_POINTER (GPOINTER_TO_INT (value));
	default:
		return g_strdup (value);
	}
}

static void
fm_free_value (ETableModel *etm, int col, void *value, gpointer data)
{
	switch (col) {
	case COL_ICON:
		if (value) gdk_pixbuf_unref (value);
		break;
	case COL_ATIME:
	case COL_CTIME:
	case COL_SIZE:
	case COL_MTIME:
		break;
	default:
		g_free (value);
	}
}

static void *
fm_initialize_value (ETableModel *etm, int col, gpointer data)
{
	switch (col) {
	case COL_ICON:
		return NULL;
	case COL_SIZE:
	case COL_CTIME:
	case COL_ATIME:
	case COL_MTIME:
		return GINT_TO_POINTER (0);
	default:
		return g_strdup ("");
	}
}

static gboolean
fm_value_is_empty (ETableModel *etm, int col, const void *value, gpointer data)
{
	switch (col) {
	case COL_ICON:
		return value == NULL;
	case COL_SIZE:
	case COL_MTIME:
	case COL_ATIME:
	case COL_CTIME:
		return GPOINTER_TO_INT (value) == 0;
	default:
		return !value || !*(char *)value;
	}
}

static char *
fm_value_to_string (ETableModel *etm, int col, const void *value, gpointer data)
{
	switch (col) {
	case COL_ICON:
		return g_strdup ("icon");
	case COL_ATIME:
	case COL_CTIME:
	case COL_MTIME:
	case COL_SIZE:
		return g_strdup_printf ("%d", GPOINTER_TO_INT (value));
#if 0
	case COL_SIZE:
		return g_strdup_printf ("%"GNOME_VFS_SIZE_FORMAT_STR, 
					GPOINTER_TO_INT (value));
#endif
	default:
		return g_strdup (value);
	}
}

static void
et_double_click (ETable *et, int row, int col, GdkEvent *event, FListTable *table)
{
	flist_dir_request (table->dir, row);
}

static void
et_selection_change (ETable *et, FListTable *table)
{
	/* do this in an idle cb? */
	flist_update_selected_count (table->flist,
				     flist_table_get_selected_count (table->widget));
}

static void
uri_changed_cb (FList *flist, const char *uri, FListTable *table)
{
	g_print ("flist-table.c: uri_changed_cb ()\n");
	/* flist_dir_set_uri (table->dir, uri); */
}

static void
flist_table_directory_loading (FListDir *dir, const char *uri, FListTable *table)
{
	flist_set_current_uri (table->flist, uri);
}

static void
uri_requested_cb (FList *flist, const char *uri, 
		  FListRequestType rtype, FListTable *table)
{
	flist_dir_request_uri (table->dir, uri, rtype);
}

static void
load_cancelled_cb (FList *flist, FListTable *table)
{
	flist_dir_cancel (table->dir);
}

static void
et_destroy (FListTable *table)
{
	g_print ("e-table destroy!\n");

	flist_dir_free (table->dir);
	gtk_object_unref (GTK_OBJECT (table->etm));

	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->uri_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->uri_requested_id);
	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->load_cancelled_id);
	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->mime_types_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->multi_select_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (table->flist),
			       table->enable_vfs_changed_id);
	
	bonobo_object_unref (BONOBO_OBJECT (table->flist));
	memset (table, 0, sizeof (FListTable));
	g_free (table);
}

static void
flist_table_files_read (FListDir *dir, int oldsize, 
			int newfiles, FListTable *table)
{
	e_table_model_rows_inserted (table->etm, oldsize, newfiles);
}

static void
flist_table_clear (FListDir *dir, int oldsize, FListTable *table)
{
	e_table_model_rows_deleted (table->etm, 0, oldsize);
}

static void
flist_table_file_activated (FListDir *dir, const char *uri, FListTable *table)
{
	flist_uris_activated (table->flist,
			      flist_table_get_selected_uris (table->widget));
}

static void
flist_table_info_changed (FListDir *dir, int idx, FListTable *table)
{
	e_table_model_row_changed (table->etm, idx);
}

static void
flist_table_file_deleted (FListDir *dir, int idx, FListTable *table)
{
	e_table_model_row_deleted (table->etm, idx);
}

static void
flist_table_report_error (FListDir *dir, const char *error, FListTable *table)
{
	flist_report_error (table->flist, error);
}

static FListDirView flist_table_view = {
	(gpointer)flist_table_directory_loading,
	(gpointer)flist_table_files_read,
	(gpointer)flist_table_clear,
	(gpointer)flist_table_file_activated,
	(gpointer)flist_table_info_changed,
	(gpointer)flist_table_file_deleted,
	(gpointer)flist_table_report_error
};

static void
mime_types_changed_cb (FList *flist, FListTable *table)
{
	flist_dir_refilter (table->dir);
}

static void
multi_select_changed_cb (FList *flist, gboolean multi_select, FListTable *table)
{
	gtk_object_set (GTK_OBJECT (table->table->selection),
			"selection_mode",
			multi_select
			? GTK_SELECTION_MULTIPLE
			: GTK_SELECTION_SINGLE,
			NULL);
}

static void
enable_vfs_changed_cb (FList *flist, gboolean enable_vfs, FListTable *table)
{
	flist_dir_set_enable_vfs (table->dir, enable_vfs);
}


GtkWidget *
flist_table_new (FList *flist)
{
	GtkWidget *ets;
	FListTable *table;
	ETable *et;
	char *uri;

	g_return_val_if_fail (flist != NULL, NULL);

	g_print ("creating table...\n");

	table = g_new0 (FListTable, 1);

	bonobo_object_ref (BONOBO_OBJECT (flist));
	table->flist = flist;

	table->dir = flist_dir_new (&flist_table_view, table);
	flist_dir_set_enable_vfs (table->dir, flist_get_vfs_enabled (flist));

	table->etm = e_table_simple_new (fm_column_count,
					 fm_row_count,
					 fm_append_row,

					 fm_value_at,
					 fm_set_value_at,
					 fm_is_cell_editable,

					 fm_has_save_id,
					 fm_get_save_id,

					 fm_duplicate_value,
					 fm_free_value,
					 fm_initialize_value,
					 fm_value_is_empty,
					 fm_value_to_string,
					 table);

	ets = e_table_scrolled_new (table->etm, NULL, list_spec, list_state);
	et  = e_table_scrolled_get_table (E_TABLE_SCROLLED (ets));

	table->widget = ets;
	table->table  = et;

	gtk_object_set_data_full (GTK_OBJECT (ets),
				  FLIST_TABLE_KEY,
				  table,
				  (GtkDestroyNotify)et_destroy);

	gtk_signal_connect (GTK_OBJECT (et), "double_click",
			    GTK_SIGNAL_FUNC (et_double_click),
			    table);

	gtk_signal_connect (GTK_OBJECT (et), "selection_change",
			    GTK_SIGNAL_FUNC (et_selection_change),
			    table);

#if 0
	gtk_signal_connect (GTK_OBJECT (et), "cursor_activated",
			    GTK_SIGNAL_FUNC (et_cursor_activated),
			    table);
#endif
	table->uri_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "uri_changed",
				    GTK_SIGNAL_FUNC (uri_changed_cb),
				    table);

	table->uri_requested_id = 
		gtk_signal_connect (GTK_OBJECT (flist), "uri_requested",
				    GTK_SIGNAL_FUNC (uri_requested_cb),
				    table);

	table->load_cancelled_id =
		gtk_signal_connect (GTK_OBJECT (flist), "load_cancelled",
				    GTK_SIGNAL_FUNC (load_cancelled_cb),
				    table);

	table->mime_types_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "mime_types_changed",
				    GTK_SIGNAL_FUNC (mime_types_changed_cb),
				    table);

	table->multi_select_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "multi_select_changed",
				    GTK_SIGNAL_FUNC (multi_select_changed_cb),
				    table);

	table->enable_vfs_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "enable_vfs_changed",
				    GTK_SIGNAL_FUNC (enable_vfs_changed_cb),
				    table);

	gtk_object_set (GTK_OBJECT (table->table->selection),
			"selection_mode",
			flist_get_multi_select (flist)
			? GTK_SELECTION_MULTIPLE
			: GTK_SELECTION_SINGLE,
			NULL);

	/* FIXME: this should be requested, or something */
	uri = flist_get_current_uri (flist);
	if (uri)
		flist_dir_request_uri (table->dir, uri, FLIST_REQUEST_ABSOLUT);
	g_free (uri);

	flist_dir_set_filter (table->dir,
			      flist_get_directory_filter (table->flist),
			      TRUE);
	return ets;
}

typedef struct {
	FListTable *table;
	CORBA_sequence_CORBA_string *seq;
	int idx;
	gboolean no_dirs;
	char *path;
} Stuff;

static void
esm_foreach (int model_row, Stuff *s)
{
	const GnomeVFSFileInfo *finfo;

	finfo = flist_dir_get_vfs_info (s->table->dir, model_row);
	
	if (s->no_dirs && FLIST_DIR_VFS_INFO_IS_DIR (finfo))
		return;

	s->seq->_buffer[s->idx++] = g_concat_dir_and_file (s->path, finfo->name);
}
	
CORBA_sequence_CORBA_string *
flist_table_get_selected_uris (GtkWidget *w)
{
	Stuff s;
		
	s.table = gtk_object_get_data (GTK_OBJECT (w), FLIST_TABLE_KEY);
	g_return_val_if_fail (s.table != NULL, NULL);

	s.seq = CORBA_sequence_CORBA_string__alloc ();
	s.seq->_release = TRUE;

	s.seq->_buffer = CORBA_sequence_CORBA_string_allocbuf (
		flist_table_get_selected_count (w));
	s.idx = 0;
	s.no_dirs = !flist_get_accept_dirs (s.table->flist);

	s.path = flist_get_current_uri (s.table->flist);

	if (!flist_get_vfs_enabled (s.table->flist)) {
		GnomeVFSURI *uri;

		uri = gnome_vfs_uri_new (s.path);
		if (uri) {
			g_free (s.path);
			s.path = g_strdup (gnome_vfs_uri_get_path (uri));
			gnome_vfs_uri_unref (uri);
		}
	}	

	e_selection_model_foreach (E_SELECTION_MODEL (s.table->table->selection),
				   (EForeachFunc)esm_foreach,
				   &s);

	s.seq->_length = s.idx;
	g_free (s.path);

	return s.seq;
}

int
flist_table_get_selected_count (GtkWidget *w)
{
	FListTable *table;

	table = gtk_object_get_data (GTK_OBJECT (w), FLIST_TABLE_KEY);
	g_return_val_if_fail (table != NULL, 0);

	return e_selection_model_selected_count (E_SELECTION_MODEL (table->table->selection));
}

static void
activate_uri (int model_row, gpointer data)
{
	FListTable *table;

	table = data;

	flist_dir_request (table->dir, model_row);
}

void
flist_table_activate_selected_uris (GtkWidget *w)
{
	ESelectionModel *esm;
	FListTable *table;

	table = gtk_object_get_data (GTK_OBJECT (w), FLIST_TABLE_KEY);
	g_return_if_fail (table != NULL);

	esm = E_SELECTION_MODEL (table->table->selection);
	switch (e_selection_model_selected_count (esm)) {
	default:
		flist_uris_activated (
			table->flist,
			flist_table_get_selected_uris (w));
		break;
	case 1:
		e_selection_model_foreach (esm, activate_uri, table);
		break;
	case 0:
		break;
	}
}
