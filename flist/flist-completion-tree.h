/*
 * flist-completion-tree.h: a tree for storing possible completions
 *
 * Authors:
 *    Jacob Berkman
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_COMPLETION_TREE_H_
#define _FLIST_COMPLETION_TREE_H_

#include <glib.h>

GNode    *flist_completion_tree_new    (void);
void      flist_completion_tree_free   (GNode *root, GFunc free_func, gpointer data);

gpointer  flist_completion_tree_insert (GNode *root, const char *s, gpointer data);
GNode    *flist_completion_tree_lookup (GNode *root, const char *s);

gpointer  flist_completion_tree_data   (GNode *node);

#endif /* _FLIST_COMPLETION_TREE_H_ */
