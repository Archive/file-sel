/*
 * flist.h: implementation of GNOME::FileList
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *    Miguel de Icaza  <miguel@ximian.com>
 *
 * Copyright 2000, 2001 Ximian, Inc.
 */

#ifndef _FILE_SEL_FLIST_H_
#define _FILE_SEL_FLIST_H_

#include "idl/GNOME_FileSelector_List.h"

#include "flist-dir.h"

#include <bonobo/bonobo-xobject.h>

#include <libgnomevfs/gnome-vfs-directory-filter.h>
#include <libgnomevfs/gnome-vfs-types.h>

#define FLIST_TYPE                (flist_get_type ())
#define FLIST(o)                  (GTK_CHECK_CAST ((o), FLIST_TYPE, FList))
#define FLIST_CLASS(k)            (GTK_CHECK_CLASS_CAST((k), FLIST_TYPE, FListClass))
#define IS_FLIST(o)               (GTK_CHECK_TYPE ((o), FLIST_TYPE))
#define IS_FLIST_CLASS(k)         (GTK_CHECK_CLASS_TYPE ((k), FLIST_TYPE))

typedef struct _FListPrivate FListPrivate;

typedef struct {
	BonoboXObject  parent;

	FListPrivate  *priv;
} FList;

typedef struct {
	BonoboXObjectClass      parent_class;

	POA_GNOME_FileSelector_List__epv epv;

	void (*uri_changed)           (FList *flist, const char *uri);
	void (*uri_requested)         (FList *flist, const char *uri, FListRequestType rtype);
	void (*load_cancelled)        (FList *flist);
	void (*mime_types_changed)    (FList *flist);
	void (*multi_select_changed)  (FList *flist, gboolean multi_select);
	void (*enable_vfs_changed)    (FList *flist, gboolean enable_vfs);
} FListClass;

GtkType       flist_get_type          (void);

void          flist_request_uri       (FList *flist, const char *uri, FListRequestType rtype);
char         *flist_get_requested_uri (FList *flist);

void          flist_set_current_uri   (FList *flist, const char *uri);
char         *flist_get_current_uri   (FList *flist);

gboolean      flist_get_multi_select  (FList *flist);
gboolean      flist_get_vfs_enabled   (FList *flist);
gboolean      flist_get_accept_dirs   (FList *flist);

void          flist_report_error      (FList *flist, const char *error);
void          flist_uris_activated    (FList *flist, CORBA_sequence_CORBA_string *seq);

void          flist_update_selected_count     (FList *flist, int count);
#if 0
void          flist_update_directory_selected (FList *flist, gboolean dir_selected);
#endif

GnomeVFSDirectoryFilter *flist_get_directory_filter (const FList *flist);

#endif /* _FILE_SEL_FLIST_H_ */
