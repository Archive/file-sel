/*
 * flist-dir.c: directory listing object
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-dir.h"
#include "widgets/flist-mime-icon.h"
#include "flist-util.h"

#include <eel/eel-vfs-extensions.h>

#include <libgnome/libgnome.h>

#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-async-ops.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-file-info.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
/* FIXME: this should not be here */
#include <libgnomevfs/gnome-vfs-ops.h>

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>

#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#define ENTRY(fd, idx) ((FListDirEntry *)((FListDir *)(fd))->priv->entries->pdata[(idx)])
#define ENTRIES(fd)    (((FListDir *)(fd))->priv->entries->len)

#define FILTERED_ENTRY(fd, idx) ((FListDirEntry *)((FListDir *)(fd))->priv->filtered_entries->pdata[(idx)])
#define FILTERED_ENTRIES(fd)    (((FListDir *)(fd))->priv->filtered_entries->len)

#define d(x)

#define view(d) (d->priv->view)

typedef struct {
	GnomeVFSFileInfo *info;
	const char       *description;
	GdkPixbuf        *pixbuf;
	char             *username;
	char             *groupname;
} FListDirEntry;

struct _FListDirPrivate {
	GPtrArray               *entries;
	GPtrArray               *filtered_entries;

	GnomeVFSAsyncHandle     *load_handle;
	GnomeVFSDirectoryFilter *filter;
	gboolean                 free_filter;

	FListDirView            *view;
	gpointer                 data;

	char                    *pending_uri;

	gboolean                 enable_vfs;
};


FListDir *
flist_dir_new (FListDirView *view, gpointer data)
{
	FListDir *dir;

	g_return_val_if_fail (view, NULL);
	g_return_val_if_fail (view->directory_loading, NULL);
	g_return_val_if_fail (view->files_read, NULL);
	g_return_val_if_fail (view->clear, NULL);
	g_return_val_if_fail (view->file_activated, NULL);
	g_return_val_if_fail (view->info_changed, NULL);
	g_return_val_if_fail (view->file_deleted, NULL);
	g_return_val_if_fail (view->report_error, NULL);

	dir = g_new0 (FListDir, 1);

	dir->priv = g_new0 (FListDirPrivate, 1);

	dir->priv->entries          = g_ptr_array_new ();
	dir->priv->filtered_entries = g_ptr_array_new ();

	dir->priv->view = view;
	dir->priv->data = data;

	dir->priv->enable_vfs = TRUE;

	return dir;
}

static void
free_entry (FListDirEntry *entry)
{
	if (entry->info)
		gnome_vfs_file_info_unref (entry->info);

	if (entry->pixbuf)
		gdk_pixbuf_unref (entry->pixbuf);

	g_free (entry->username);
	g_free (entry->groupname);

	memset (entry, 0, sizeof (FListDirEntry));
	g_free (entry);
}

static void
free_entries (FListDir *fd)
{
	int i;

	for (i = 0; i < ENTRIES (fd); i++) {
		free_entry (ENTRY (fd, i));
		ENTRY (fd, i) = NULL;
	}

	/* FIXME: if the size is large, we should like free the
	 * thing */
	g_ptr_array_set_size (fd->priv->entries, 0); 
	g_ptr_array_set_size (fd->priv->filtered_entries, 0);
}

static void
clear_dir (FListDir *dir)
{
	int rows = FILTERED_ENTRIES (dir);
	free_entries (dir);
	view (dir)->clear (dir, rows, dir->priv->data);
}
void
flist_dir_free (FListDir *dir)
{
	g_return_if_fail (dir != NULL);

	flist_dir_cancel (dir);
	free_entries (dir);
	g_ptr_array_free (dir->priv->entries, FALSE);
	g_ptr_array_free (dir->priv->filtered_entries, FALSE);
	if (dir->priv->free_filter)
		gnome_vfs_directory_filter_destroy (dir->priv->filter);
	g_free (dir->priv->pending_uri);
	g_free (dir->priv);
	g_free (dir->uri);
}

static void
update_entry_info (FListDir *dir, FListDirEntry *entry)
{
	char *file;
	const char *mime_type = NULL;

	if (!(entry->info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE))
		g_warning ("hmm.\n");

	/* we can totally do this later, which should happen for the
	 * umm completion stuff */
	switch (entry->info->type) {
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		entry->description = _("Folder");
		break;
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
	case GNOME_VFS_FILE_TYPE_REGULAR:
	case GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK:
		mime_type = gnome_vfs_mime_type_from_name (entry->info->name);
		entry->description = gnome_vfs_mime_get_description (mime_type);
		break;
	default:
		entry->description = _("Other");
		break;
	}

	file = g_concat_dir_and_file (dir->uri, entry->info->name);
	entry->pixbuf = flist_mime_icon_load (file, entry->info->type, mime_type);
	g_free (file);
}

static FListDirEntry *
new_entry (FListDir *dir, GnomeVFSFileInfo *finfo)
{
	FListDirEntry *entry;

	entry = g_new0 (FListDirEntry, 1);

	gnome_vfs_file_info_ref (finfo);
	entry->info = finfo;

	update_entry_info (dir, entry);

	return entry;
}

static void
cleanup_load (FListDir *dir, gboolean success)
{
	if (success) {
		int len;
		char *uri;

		g_free (dir->uri);

		uri = dir->priv->pending_uri;
		len = strlen (uri);
		if (uri[len - 1] != '/') {
			g_message ("appending \"/\"");
			dir->uri = g_strdup_printf ("%s/", uri);
			g_free (uri);
		} else {
			dir->uri = uri;
		}
	} else {
		g_free (dir->priv->pending_uri);
	}

	dir->priv->pending_uri = NULL;
}

static void
do_filter (FListDir *dir, int oldsize, int newrows)
{
	int filtered_oldsize, filtered_newrows;
	int i;

#if 0 /* we still need to copy unfiltered array to filtered array */
	if (!dir->priv->filter) {
		g_print ("skipping filter!\n");
		view (dir)->files_read (dir, oldsize, newrows, dir->priv->data);
		return;
	}
#endif

	filtered_oldsize = FILTERED_ENTRIES (dir);
	filtered_newrows = 0;

	for (i = oldsize; i < oldsize + newrows; i++) {
		if (gnome_vfs_directory_filter_apply (dir->priv->filter,
						      ENTRY (dir, i)->info)) {
			g_ptr_array_add (dir->priv->filtered_entries,
					 ENTRY (dir, i));
			filtered_newrows++;
		}
	}

	if (filtered_newrows)
		view (dir)->files_read (dir, filtered_oldsize, 
					filtered_newrows, dir->priv->data);
}

void
flist_dir_refilter (FListDir *dir)
{
	view (dir)->clear (dir, FILTERED_ENTRIES (dir), dir->priv->data);
	g_ptr_array_set_size (dir->priv->filtered_entries, 0);
	do_filter (dir, 0, ENTRIES (dir));
}

static void
dir_load_cb (GnomeVFSAsyncHandle *handle, GnomeVFSResult result,
	     GList *list, guint entries_read, FListDir *dir)
{
	GList *li;
	int i, oldsize, newrows;
	char *s;

	d(g_message ("dir_load_cb()"));

	switch (result) {
	case GNOME_VFS_ERROR_EOF:
		dir->priv->load_handle = NULL;
		/* fall thru */
	case GNOME_VFS_OK:
		if (!dir->priv->pending_uri)
			break;

		view (dir)->directory_loading (dir, dir->priv->pending_uri,
					       dir->priv->data);

		clear_dir (dir);
		cleanup_load (dir, TRUE);
		break;
	case GNOME_VFS_ERROR_NOT_A_DIRECTORY:
	case GNOME_VFS_ERROR_GENERIC:
		g_message ("%s is not a dir", dir->priv->pending_uri);
		view (dir)->file_activated (dir, dir->priv->pending_uri, dir->priv->data);
		cleanup_load (dir, FALSE);
		return;
	default:
		dir->priv->load_handle = NULL;
		/* FIXME: better error reporting */
		s = g_strdup_printf ("There was an error loading `%s':\n\n%s",
				     dir->priv->pending_uri,
				     gnome_vfs_result_to_string (result));
		view (dir)->report_error (dir, s, dir->priv->data);
		g_free (s);
		cleanup_load (dir, FALSE);
		return;
	}

	oldsize = ENTRIES (dir);
	newrows = g_list_length (list);
	g_ptr_array_set_size (dir->priv->entries, oldsize + newrows);

	for (i = oldsize, li = list; li; li = li->next, i++)
		ENTRY (dir, i) = new_entry (dir, li->data);

	do_filter (dir, oldsize, newrows);

	/* dir_done can be NULL for now */
	if (result == GNOME_VFS_ERROR_EOF &&
	    view (dir)->directory_done)
		view (dir)->directory_done (dir, dir->priv->data);
}


static gboolean
is_remote (const char *uri)
{
	GnomeVFSURI *vfs_uri;
	gboolean retval;

	vfs_uri = gnome_vfs_uri_new (uri);
	retval = strcmp (gnome_vfs_uri_get_scheme (vfs_uri), "file");
	gnome_vfs_uri_unref (vfs_uri);

	return retval;
}

static void
flist_dir_try_pending (FListDir *dir)
{
	g_print ("trying: `%s'\n", dir->priv->pending_uri);

	if (!dir->priv->enable_vfs &&
	    is_remote (dir->priv->pending_uri)) {
		view (dir)->report_error (dir,
					  "The requested file cannot be opened by this application.",
					  dir->priv->data);
		cleanup_load (dir, FALSE);
		return;
	}

	gnome_vfs_async_load_directory (&dir->priv->load_handle,
					dir->priv->pending_uri,
					GNOME_VFS_FILE_INFO_FOLLOW_LINKS,
					GNOME_VFS_DIRECTORY_FILTER_NONE,
					GNOME_VFS_DIRECTORY_FILTER_NODOTFILES,
					NULL,
					25,
					(gpointer)dir_load_cb,
					dir);
}

void
flist_dir_request (FListDir *dir, int i)
{
	const GnomeVFSFileInfo *info;

	g_return_if_fail (i >= 0);
	g_return_if_fail (dir != NULL);
	g_return_if_fail (i < FILTERED_ENTRIES (dir));

	flist_dir_cancel (dir);

	info = FILTERED_ENTRY (dir, i)->info;

	if (!(info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE) ||
	    FLIST_DIR_VFS_INFO_IS (info, 
				   GNOME_VFS_FILE_TYPE_DIRECTORY | 
				   GNOME_VFS_FILE_TYPE_UNKNOWN)) {
		flist_dir_request_uri (dir, info->name, FLIST_REQUEST_RELATIVE);
	} else {
		char *s;
		s = eel_uri_make_full_from_relative (dir->uri, info->name);
		view (dir)->file_activated (dir, s, dir->priv->data);
		g_free (s);
	}
}

void
flist_dir_request_uri (FListDir *dir, const char *uri, FListRequestType rtype)
{
	g_return_if_fail (dir != NULL);
	g_return_if_fail (uri != NULL);

	g_print ("flist_dir_request_uri(): %s\n", uri);

	flist_dir_cancel (dir);

	if (rtype == FLIST_REQUEST_TRY_RELATIVE)
		rtype = (*uri == '/' || *uri == '~')
			? FLIST_REQUEST_ABSOLUT
			: FLIST_REQUEST_RELATIVE;

	g_print ("starting with: %s, %s\n", dir->uri, uri);

	dir->priv->pending_uri = 
		(rtype == FLIST_REQUEST_RELATIVE && dir->uri && strlen (dir->uri))
		? eel_uri_make_full_from_relative (dir->uri, uri)
		: eel_make_uri_from_input (uri);

	g_print ("got: %s\n", dir->priv->pending_uri);
		
	flist_dir_try_pending (dir);
}

void
flist_dir_cancel (FListDir *dir)
{
	g_return_if_fail (dir != NULL);
	if (!dir->priv->load_handle)
		return;

	if (view (dir)->load_cancelled)
		view (dir)->load_cancelled (dir, dir->priv->data);

	gnome_vfs_async_cancel (dir->priv->load_handle);
	dir->priv->load_handle = NULL;

	cleanup_load (dir, FALSE);
}

void
flist_dir_dump_infos (const FListDir *dir)
{
	int i;

	g_return_if_fail (dir != NULL);
	
	g_print ("dumping file infos for: %s\n", dir->uri);
	for (i = 0; i < ENTRIES (dir); i++)
		g_print ("\t%d: %s (%d)\n", i,
			 ENTRY (dir, i)->info->name,
			 gnome_vfs_directory_filter_apply (
				 dir->priv->filter, ENTRY (dir, i)->info));
}

int
flist_dir_get_size (const FListDir *dir)
{
	g_return_val_if_fail (dir != NULL, 0);

	return FILTERED_ENTRIES (dir);
}

const GnomeVFSFileInfo *
flist_dir_get_vfs_info (const FListDir *dir, int i)
{
	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);

	return FILTERED_ENTRY (dir, i)->info;
}

const char *
flist_dir_get_description (const FListDir *dir, int i)
{
	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);

	return FILTERED_ENTRY (dir, i)->description;
}

const GdkPixbuf *
flist_dir_get_pixbuf (const FListDir *dir, int i)
{
	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);

	return FILTERED_ENTRY (dir, i)->pixbuf;
}

const char *
flist_dir_get_username (const FListDir *dir, int i)
{
	FListDirEntry *entry;

	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);

	entry = FILTERED_ENTRY (dir, i);

	if (!entry->username)
		entry->username = g_strdup (
			flist_util_get_username (entry->info->uid));

	return entry->username;
}

const char *
flist_dir_get_groupname (const FListDir *dir, int i)
{
	FListDirEntry *entry;

	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);

	entry = FILTERED_ENTRY (dir, i);

	if (!entry->groupname)
		entry->groupname = g_strdup (
			flist_util_get_groupname (entry->info->gid));

	return entry->groupname;
}

void
flist_dir_set_filter (FListDir *dir, GnomeVFSDirectoryFilter *filter, gboolean free_filter)
{
	g_return_if_fail (dir != NULL);

	if (dir->priv->free_filter)
		gnome_vfs_directory_filter_destroy (dir->priv->filter);

	dir->priv->filter = filter;
	dir->priv->free_filter = free_filter;

	flist_dir_refilter (dir);
}

static char *
flist_dir_append_path (const FListDir *dir, const char *filename)
{
	g_return_val_if_fail (dir->uri != NULL, NULL);

	return g_concat_dir_and_file (dir->uri, filename);
}

void
flist_dir_rename (FListDir *dir, int i, const char *new_name)
{
	char *src, *dst;

	g_return_if_fail (i >= 0);
	g_return_if_fail (dir != NULL);
	g_return_if_fail (i < FILTERED_ENTRIES (dir));
	
	src = flist_dir_get_path (dir, i);
	dst = flist_dir_append_path (dir, new_name);

	/* FIXME: bugzilla.gnome.org 58633 no sync vfs calls */
	/* so theoretically, if the file type changes, it should be
	 * refiltered, but this kinda sucks so we will just not do
	 * that for now. */
	if (GNOME_VFS_OK == gnome_vfs_move (src, dst, FALSE)) {
		FListDirEntry *entry;

		entry = FILTERED_ENTRY (dir, i);
		gnome_vfs_get_file_info (dst, 
					 FILTERED_ENTRY (dir, i)->info,
					 GNOME_VFS_FILE_INFO_FOLLOW_LINKS);

		if (entry->pixbuf) {
			gdk_pixbuf_unref (entry->pixbuf);
			entry->pixbuf = NULL;
		}
		entry->description = NULL;

		update_entry_info (dir, entry);

		view (dir)->info_changed (dir, i, dir->priv->data);
	}
}

void
flist_dir_delete (FListDir *dir, int i)
{
	g_return_if_fail (i >= 0);
	g_return_if_fail (dir != NULL);
	g_return_if_fail (i < FILTERED_ENTRIES (dir));

	g_warning ("IMPLEMENT ME");
}

char *
flist_dir_get_path (const FListDir *dir, int i)
{
	g_return_val_if_fail (i >= 0,      NULL);
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (i < FILTERED_ENTRIES (dir), NULL);
	
	return flist_dir_append_path (dir, FILTERED_ENTRY (dir, i)->info->name);
}

/* FIXME: i suppose we should do something if we have a remote uri
 * loaded and this is set to FALSE? */
void
flist_dir_set_enable_vfs (FListDir *dir, gboolean enable_vfs)
{
	g_return_if_fail (dir != NULL);

	dir->priv->enable_vfs = enable_vfs;
}

gboolean
flist_dir_get_enable_vfs (FListDir *dir)
{
	g_return_val_if_fail (dir != NULL, FALSE);

	return dir->priv->enable_vfs;
}
