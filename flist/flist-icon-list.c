/*
 * flist-icon-list.h: list view for the flist
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-icon-list.h"
#include "flist-dir.h"
#include "widgets/gnome-icon-list-hack.h"

#include <eel/eel-vfs-extensions.h>

#include <gal/widgets/e-scroll-frame.h>

#include <gdk/gdkkeysyms.h>

#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksignal.h>

#include <libgnome/libgnome.h>

#define FLIST_ICON_LIST_KEY "FListIconList"

typedef struct {
	GtkWidget         *widget;
	FList             *flist;
	FListDir          *dir;
	GnomeIconListHack *gil;
	guint              uri_changed_id;
	guint              uri_requested_id;
	guint              load_cancelled_id;
	guint              mime_types_changed_id;
	guint              multi_select_changed_id;
	guint              enable_vfs_changed_id;
} FListIconList;

static void
destroy_cb (FListIconList *list)
{
	g_print ("icon list destroy!\n");

	flist_dir_free (list->dir);
	
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->uri_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->uri_requested_id);
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->load_cancelled_id);
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->mime_types_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->multi_select_changed_id);
	gtk_signal_disconnect (GTK_OBJECT (list->flist),
			       list->enable_vfs_changed_id);

	bonobo_object_unref (BONOBO_OBJECT (list->flist));

	memset (list, 0, sizeof (FListIconList));
	g_free (list);	
}

static void
update_selected_count (FListIconList *list)
{
	flist_update_selected_count (list->flist,
				     flist_icon_list_get_selected_count (list->widget));
}

static void
select_icon_cb (GnomeIconListHack *gil, gint num, GdkEvent *event, FListIconList *list)
{
	update_selected_count (list);

	if (event && 
	    (event->type == GDK_2BUTTON_PRESS) && 
	    (((GdkEventButton *)event)->button == 1)) {
		flist_dir_request (list->dir, num);
	}
}

static void
unselect_icon_cb (GnomeIconListHack *gil, gint num, GdkEvent *event, FListIconList *list)
{
	update_selected_count (list);
}

static gboolean 
button_press_cb (GtkWidget *widget, GdkEventButton *button, gpointer data)
{
	if (button->button == 4 || button->button == 5) {
		GtkAdjustment *adj = GNOME_ICON_LIST_HACK (widget)->adj;
		gfloat new_value = adj->value + ((button->button == 4) ?
						 - adj->page_increment / 2:
						 adj->page_increment / 2);
		new_value = CLAMP (new_value, adj->lower, adj->upper - adj->page_size);
		gtk_adjustment_set_value (adj, new_value);
		
		return TRUE;
	}

	return FALSE;
}

static int
key_press_cb (GtkWidget *widget, GdkEventKey *event, FListIconList *list)
{
	g_print ("finally got event\n");

	if (!list->gil->selection)
		return FALSE;

	switch (event->keyval) {
	case GDK_Delete:
		
		break;
	case GDK_ISO_Enter:
	case GDK_KP_Enter:
	case GDK_Return:
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

static void
uri_changed_cb (FList *flist, const char *uri, FListIconList *list)
{
	g_print ("flist-icon-list.c: uri_changed_cb ()\n");
	/* flist_dir_set_uri (list->dir, uri); */
}



static void
uri_requested_cb (FList *flist, const char *uri,
		  FListRequestType rtype, FListIconList *list)
{
	flist_dir_request_uri (list->dir, uri, rtype);
}

static void
load_cancelled_cb (FList *flist, FListIconList *list)
{
	flist_dir_cancel (list->dir);
}

/*
 * Creates a 24-bits RGB value from a GdkColor
 */
static guint
rgb_from_gdk_color (GdkColor *color)
{
	guint a =
		(((color->red >> 8) << 16) |
		 ((color->green >> 8) << 8) |
		 ((color->blue >> 8)));
	
	return a;
}

static GnomeCanvasItem *
flatten_alpha (const GdkPixbuf *image, GnomeCanvas *canvas)
{
	GnomeCanvasItem *item;
	GtkStyle *style;
	GdkPixbuf *flat;
	guint rgb;
	
	if (!image)
		return NULL;

	if (gdk_pixbuf_get_has_alpha (image)) {

		if (!GTK_WIDGET_REALIZED (GTK_WIDGET (canvas)))
			gtk_widget_realize (GTK_WIDGET (canvas));
		
		style = gtk_widget_get_style (GTK_WIDGET (canvas));
		rgb = rgb_from_gdk_color (&style->base[GTK_STATE_NORMAL]);
		
		flat = gdk_pixbuf_composite_color_simple (
			image,
			gdk_pixbuf_get_width (image),
			gdk_pixbuf_get_height (image),
			GDK_INTERP_NEAREST,
			255,
			32,
			rgb, rgb);
	} else {
		/* maybe we should _copy() instead ? */
		flat = image;
		gdk_pixbuf_ref (flat);
	}

	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (canvas->root),
				      GNOME_TYPE_CANVAS_PIXBUF,
				      "pixbuf", flat, 
				      "height", (double)gdk_pixbuf_get_height (flat),
				      "width", (double)gdk_pixbuf_get_width (flat),
				      NULL);

	gdk_pixbuf_unref (flat);

	return item;
}

static void
append_icon (GnomeIconListHack *gil, FListDir *dir, int i)
{
	const GdkPixbuf *pb;
	const GnomeVFSFileInfo *info;
	
	pb   = flist_dir_get_pixbuf   (dir, i);
	info = flist_dir_get_vfs_info (dir, i);

	gnome_icon_list_hack_append_item (
		gil,
		flatten_alpha (pb, GNOME_CANVAS (gil)),
		info->name);
}

static void
flist_icon_list_directory_loading (FListDir *dir, const char *uri, FListIconList *list)
{
	flist_set_current_uri (list->flist, uri);
}

static void
flist_icon_list_files_read (FListDir *dir, int oldsize,
			    int newfiles, FListIconList *list)
{
	int i;

	if (!GTK_WIDGET_REALIZED (list->gil))
		return;

	gnome_icon_list_hack_freeze (list->gil);

	for (i = oldsize; i < oldsize + newfiles; i++) 
		append_icon (list->gil, list->dir, i);

	gnome_icon_list_hack_thaw (list->gil);
}

static void
flist_icon_list_clear (FListDir *dir, int oldsize, FListIconList *list)
{
	gnome_icon_list_hack_clear (list->gil);
}

static void
flist_icon_list_file_activated (FListDir *dir, const char *uri, FListIconList *list)
{
	flist_uris_activated (list->flist,
			      flist_icon_list_get_selected_uris (list->widget));
}

typedef struct {
	FListIconList *list;
	int idx;
} InfoChangedData;

static void
info_changed_cb (InfoChangedData *data)	
{
	FListIconList *list;
	FListDir *dir;
	const GdkPixbuf *pb;
	const GnomeVFSFileInfo *info;
	int idx;

	list = data->list;
	idx = data->idx;
	dir = list->dir;

	gnome_icon_list_hack_remove (list->gil, idx);

	pb   = flist_dir_get_pixbuf (dir, idx);
	info = flist_dir_get_vfs_info (dir, idx);

	gnome_icon_list_hack_insert_item (
		list->gil, idx,
		flatten_alpha (pb, GNOME_CANVAS (list->gil)),
		info->name);
	
	g_free (data);
}

static void
flist_icon_list_info_changed (FListDir *dir, int idx, FListIconList *list)
{
	InfoChangedData *data;
	
	data = g_new (InfoChangedData, 1);
	data->list = list;
	data->idx = idx;

	g_print ("changing dingus...\n");

	g_idle_add ((GSourceFunc)info_changed_cb, data);
}

static void
flist_icon_list_file_deleted (FListDir *dir, int idx, FListIconList *list)
{
	gnome_icon_list_hack_remove (list->gil, idx);
}

static void
flist_icon_list_report_error (FListDir *dir, const char *error, FListIconList *list)
{
	flist_report_error (list->flist, error);
}

static FListDirView flist_icon_list_view = {
	(gpointer)flist_icon_list_directory_loading,
	(gpointer)flist_icon_list_files_read,
	(gpointer)flist_icon_list_clear,
	(gpointer)flist_icon_list_file_activated,
	(gpointer)flist_icon_list_info_changed,
	(gpointer)flist_icon_list_file_deleted,
	(gpointer)flist_icon_list_report_error
};

static void
realize_cb (GtkObject *o, FListIconList *list)
{
	int i, size = flist_dir_get_size (list->dir);

	if (!size)
		return;

	gnome_icon_list_hack_freeze (list->gil);

	for (i = 0; i < size; i++)
		append_icon (list->gil, list->dir, i);

	gnome_icon_list_hack_thaw (list->gil);
}

static void
mime_types_changed_cb (FList *flist, FListIconList *list)
{
	flist_dir_refilter (list->dir);
}

static int
icon_editable_cb (GnomeIconListHack *gil, gint idx, FListIconList *list)
{
	return TRUE;
}

static gboolean
text_changed_cb (GnomeIconListHack *gil, gint idx, const char *new_text, FListIconList *list)
{
	flist_dir_rename (list->dir, idx, new_text);
	return FALSE;
}

static void
multi_select_changed_cb (FList *flist, gboolean multi_select, FListIconList *list)
{
	gnome_icon_list_hack_set_selection_mode (list->gil,
						 multi_select
						 ? GTK_SELECTION_MULTIPLE
						 : GTK_SELECTION_SINGLE);
}

static void
enable_vfs_changed_cb (FList *flist, gboolean enable_vfs, FListIconList *list)
{
	flist_dir_set_enable_vfs (list->dir, enable_vfs);
}

GtkWidget *
flist_icon_list_new (FList *flist)
{
	GtkWidget *icon_list;
	GtkWidget *scroll;
	GtkAdjustment *adj;
	FListIconList *list;
	char *uri;

	g_print ("creating icon list\n");

	list = g_new0 (FListIconList, 1);

	bonobo_object_ref (BONOBO_OBJECT (flist));
	list->flist = flist;

	list->dir = flist_dir_new (&flist_icon_list_view, list);
	
	flist_dir_set_enable_vfs (list->dir, flist_get_vfs_enabled (flist));

	scroll = e_scroll_frame_new (NULL, NULL);

	e_scroll_frame_set_shadow_type (E_SCROLL_FRAME (scroll), GTK_SHADOW_IN);
	e_scroll_frame_set_policy (E_SCROLL_FRAME (scroll),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_ALWAYS);

	adj = e_scroll_frame_get_vadjustment (E_SCROLL_FRAME (scroll));

	icon_list = gnome_icon_list_hack_new (64, adj, GNOME_ICON_LIST_HACK_IS_EDITABLE);

	list->gil = GNOME_ICON_LIST_HACK (icon_list);

	gtk_object_set_data_full (GTK_OBJECT (scroll),
				  FLIST_ICON_LIST_KEY,
				  list, 
				  (GtkDestroyNotify)destroy_cb);

	/* we can't add icons until we are realized.
	 * this is a nice hack!
	 */
	gtk_signal_connect_after (GTK_OBJECT (icon_list), "realize",
				  GTK_SIGNAL_FUNC (realize_cb), list);

	gtk_signal_connect (GTK_OBJECT (icon_list), "button_press_event",
			    GTK_SIGNAL_FUNC (button_press_cb), NULL);
	gtk_signal_connect (GTK_OBJECT (icon_list), "key_press_event",
			    GTK_SIGNAL_FUNC (key_press_cb), list);

	gtk_signal_connect (GTK_OBJECT (icon_list), "select_icon",
			    GTK_SIGNAL_FUNC (select_icon_cb), list);
	gtk_signal_connect (GTK_OBJECT (icon_list), "unselect_icon",
			    GTK_SIGNAL_FUNC (unselect_icon_cb), list);

	gtk_signal_connect (GTK_OBJECT (icon_list), "icon_editable",
			    GTK_SIGNAL_FUNC (icon_editable_cb), list);
	gtk_signal_connect (GTK_OBJECT (icon_list), "text_changed",
			    GTK_SIGNAL_FUNC (text_changed_cb), list);
	
	list->uri_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "uri_changed",
				    GTK_SIGNAL_FUNC (uri_changed_cb),
				    list);
	list->uri_requested_id =
		gtk_signal_connect (GTK_OBJECT (flist), "uri_requested",
				    GTK_SIGNAL_FUNC (uri_requested_cb),
				    list);

	list->load_cancelled_id =
		gtk_signal_connect (GTK_OBJECT (flist), "load_cancelled",
				    GTK_SIGNAL_FUNC (load_cancelled_cb),
				    list);

	list->mime_types_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "mime_types_changed",
				    GTK_SIGNAL_FUNC (mime_types_changed_cb),
				    list);

	list->multi_select_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "multi_select_changed",
				    GTK_SIGNAL_FUNC (multi_select_changed_cb),
				    list);

	list->enable_vfs_changed_id =
		gtk_signal_connect (GTK_OBJECT (flist), "enable_vfs_changed",
				    GTK_SIGNAL_FUNC (enable_vfs_changed_cb),
				    list);

	gnome_icon_list_hack_set_selection_mode (list->gil,
						 flist_get_multi_select (flist)
						 ? GTK_SELECTION_MULTIPLE
						 : GTK_SELECTION_SINGLE);

	gtk_container_add (GTK_CONTAINER (scroll), icon_list);

	/* FIXME: this should be requested or something */
	uri = flist_get_current_uri (flist);
	if (uri)
		flist_dir_request_uri (list->dir, uri, FLIST_REQUEST_ABSOLUT);
	g_free (uri);

	flist_dir_set_filter (list->dir,
			      flist_get_directory_filter (list->flist),
			      TRUE);
	list->widget = scroll;

	return scroll;
}

CORBA_sequence_CORBA_string *
flist_icon_list_get_selected_uris (GtkWidget *w)
{
	const GnomeVFSFileInfo *finfo;
	FListIconList *list;
	CORBA_sequence_CORBA_string *seq;
	GList *item;
	int i, j;
	char *current;

	list = gtk_object_get_data (GTK_OBJECT (w), FLIST_ICON_LIST_KEY);
	g_return_val_if_fail (list != NULL, NULL);

	seq = CORBA_sequence_CORBA_string__alloc ();
	seq->_release = TRUE;

	seq->_buffer = CORBA_sequence_CORBA_string_allocbuf (
		flist_icon_list_get_selected_count (w));

	current = flist_get_current_uri (list->flist);

	if (!flist_get_vfs_enabled (list->flist)) {
		GnomeVFSURI *uri;

		uri = gnome_vfs_uri_new (current);
		if (uri) {
			g_free (current);
			current = g_strdup (gnome_vfs_uri_get_path (uri));
			gnome_vfs_uri_unref (uri);
		}
	}

	for (i=j=0, item = list->gil->selection; item; i++, item=item->next) {
		finfo = flist_dir_get_vfs_info (list->dir, 
						GPOINTER_TO_INT (item->data));
		if (flist_get_accept_dirs (list->flist) && 
		    FLIST_DIR_VFS_INFO_IS_DIR (finfo))
			continue;

		seq->_buffer[j++] = g_concat_dir_and_file (current, finfo->name);
	}

	seq->_length = j;

	g_free (current);

	return seq;
}

int
flist_icon_list_get_selected_count (GtkWidget *w)
{
	FListIconList *list;

	list = gtk_object_get_data (GTK_OBJECT (w), FLIST_ICON_LIST_KEY);
	g_return_val_if_fail (list != NULL, 0);

	/* we could hack gil to keep this as an int.  as in - this is
	 * pretty slow if we call it each time the selection
	 * changes */
	return g_list_length (list->gil->selection);
}

void
flist_icon_list_activate_selected_uris (GtkWidget *w)
{
	FListIconList *list;

	list = gtk_object_get_data (GTK_OBJECT (w), FLIST_ICON_LIST_KEY);
	g_return_if_fail (list != NULL);

	switch (g_list_length (list->gil->selection)) {
	default:
		flist_uris_activated (
			list->flist,
			flist_icon_list_get_selected_uris (w));
		break;
	case 1:
		flist_dir_request (
			list->dir, 
			GPOINTER_TO_INT (list->gil->selection->data));
		break;
	case 0:
		break;
	}
}
