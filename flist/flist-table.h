/*
 * flist-table.c: table view for the flist
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_TABLE_H_
#define _FLIST_TABLE_H_

#include "flist.h"
#include <gtk/gtkwidget.h>

GtkWidget *flist_table_new (FList *flist);

int flist_table_get_selected_count (GtkWidget *w);

CORBA_sequence_CORBA_string *flist_table_get_selected_uris (GtkWidget *w);

void flist_table_activate_selected_uris (GtkWidget *w);

#endif /* _FLIST_TABLE_H_ */
