/*
 * flist.c: implementation of GNOME::FileList
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *    Miguel de Icaza  <miguel@ximian.com>
 *
 * Copyright 2000, 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist.h"
#include "flist-icon-list.h"
#include "flist-table.h"

#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-shlib-factory.h>

#include <gal/e-text/e-entry.h>

#include <gtk/gtkeventbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtksignal.h>

#include <libgnome/libgnome.h>

#include <libgnomeui/gnome-pixmap.h>
#include <libgnomeui/gnome-uidefs.h>

#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

#define d(x)

enum {
	PROP_ACCEPT_DIRS,
	PROP_CURRENT_WORKING_URI,
	PROP_ENABLE_VFS,
	PROP_MIME_TYPES,
	PROP_MULTI_SELECT,
	PROP_REQUESTED_URI,
	PROP_SELECTED_URI_COUNT,
	PROP_VIEW,
	PROP_LAST
};

static const char *prop_name[] = {
	"AcceptDirectories",
	"CurrentWorkingURI",
	"EnableVFS",
	"MimeTypes",
	"MultipleSelection",
	"RequestedURI",
	"SelectedURICount",
	"View"
};

static const char *prop_desc[] = {
	N_("Calling application can accept directories"),
	N_("URI currently displayed"),
	N_("Allow gnome-vfs aware URIs"),
	N_("Mime types displayed"),
	N_("Enable multiple selection of files"),
	N_("URI currently being requested"),
	N_("Number of URIs selected"),
	N_("View mode")
};

enum {
	SIGNAL_URI_CHANGED,
	SIGNAL_URI_REQUESTED,
	SIGNAL_ENABLE_VFS_CHANGED,
	SIGNAL_LOAD_CANCELLED,
	SIGNAL_MIME_TYPES_CHANGED,
	SIGNAL_MULTI_SELECT_CHANGED,
	SIGNAL_LAST
};

typedef enum {
	VIEW_DETAILS,
	VIEW_ICONS,
#if 0
	VIEW_TREE,
	VIEW_LISTS,
#endif
	VIEW_LAST
} FListView;


static guint flist_signal[SIGNAL_LAST] = { 0 };

static GtkObjectClass *parent_class;

const char *view_name[] = {
	N_("Details"),
	N_("Icons"),
	N_("Tree"),
	N_("Lists")
};

const char *view_icon[] = {
	ICONDIR"/html-view.xpm",
	ICONDIR"/icon-view.xpm",
	ICONDIR"/tree-view.xpm",
	ICONDIR"/html-view.xpm"
};

struct _FListPrivate {
	BonoboPropertyBag *pb;
	BonoboEventSource *es;

	GtkWidget     *w_container;
	GtkWidget     *w_view;

	GtkWidget     *type_combo;
	BonoboObject  *type_control;
	
	FListView      view;

	gboolean       multi_select;

	char          *current_uri;

	char         **mime_types;
	gboolean       accept_dirs;
	gboolean       enable_vfs;

	char            *requested_uri;
	FListRequestType requested_uri_type;
};

static gboolean
flist_directory_filter (const GnomeVFSFileInfo *info, const FList *flist)
{
	const char *mime_type;
	int i;

	d(g_print ("flist_directory_filter()\n"));

	if (!flist->priv->mime_types)
		return TRUE;

	switch (info->type) {
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		return TRUE;
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
	case GNOME_VFS_FILE_TYPE_REGULAR:
		mime_type = gnome_vfs_mime_type_from_name (info->name);
		if (!mime_type)
			return FALSE;
		for (i = 0; flist->priv->mime_types[i]; i++) {
			d(g_print ("comparing: %s ?= %s\n", 
				   flist->priv->mime_types[i], mime_type));
			if (!strcmp (flist->priv->mime_types[i], mime_type))
				return TRUE;
		}
		break;
	default:
		break;
	}
	return FALSE;
}

void
flist_report_error (FList *flist, const char *error)
{
	BonoboArg *arg;

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, error);
	bonobo_event_source_notify_listeners_full (flist->priv->es,
						   "GNOME/FileSelector/List",
						   "Error",
						   NULL,
						   arg, NULL);
	bonobo_arg_release (arg);
}

gboolean
flist_get_multi_select (FList *flist)
{
	return flist->priv->multi_select;
}

gboolean
flist_get_accept_dirs (FList *flist)
{
	return flist->priv->accept_dirs;
}

gboolean
flist_get_vfs_enabled (FList *flist)
{
	return flist->priv->enable_vfs;
}

GnomeVFSDirectoryFilter *
flist_get_directory_filter (const FList *flist)
{
	return gnome_vfs_directory_filter_new_custom (
		(gpointer)flist_directory_filter,
		GNOME_VFS_DIRECTORY_FILTER_NEEDS_NAME |
		GNOME_VFS_DIRECTORY_FILTER_NEEDS_TYPE,
		(gpointer)flist);
}

char *
flist_get_requested_uri (FList *flist)
{
	g_return_val_if_fail (IS_FLIST (flist), NULL);

	return g_strdup (flist->priv->requested_uri);
}

char *
flist_get_current_uri (FList *flist)
{
	g_return_val_if_fail (IS_FLIST (flist), NULL);

	return g_strdup (flist->priv->current_uri);
}

void
flist_request_uri (FList *flist, const char *uri, FListRequestType rtype)
{
	g_return_if_fail (IS_FLIST (flist));
	g_return_if_fail (uri != NULL);

	g_free (flist->priv->requested_uri);
	flist->priv->requested_uri = g_strdup (uri);
	flist->priv->requested_uri_type = rtype;

	gtk_signal_emit (GTK_OBJECT (flist), 
			 flist_signal[SIGNAL_URI_REQUESTED], 
			 flist->priv->requested_uri,
			 rtype);
}

void
flist_set_current_uri (FList *flist, const char *uri)
{
	int len;

	g_return_if_fail (IS_FLIST (flist));
	g_return_if_fail (uri != NULL);

	/* FIXME: is this a race condition? */
	g_free (flist->priv->requested_uri);
	flist->priv->requested_uri = NULL;

	g_free (flist->priv->current_uri);

	len = strlen (uri);
	flist->priv->current_uri = 
		g_strconcat (uri, uri[len - 1] == '/' ? NULL : "/", NULL);

	gtk_signal_emit (GTK_OBJECT (flist), 
			 flist_signal[SIGNAL_URI_CHANGED], 
			 flist->priv->current_uri);
}

static void
impl_cancelLoading (PortableServer_Servant servant,
		    CORBA_Environment *ev)
{
	FList *flist = FLIST (BONOBO_X_SERVANT_GET_OBJECT (servant));
	g_return_if_fail (IS_FLIST (flist));

	gtk_signal_emit (GTK_OBJECT (flist), flist_signal[SIGNAL_LOAD_CANCELLED]);
}

static void
flist_set_view (FList *flist, FListView view)
{
	d(g_print ("setting view: %d (%d, %p)\n", view, 
		   flist->priv->view, flist->priv->w_view));

	if (flist->priv->w_view && (flist->priv->view == view))
		return;

	if (flist->priv->w_view)
		gtk_container_remove (GTK_CONTAINER (flist->priv->w_container),
				      flist->priv->w_view);

	switch (view) {
	case VIEW_DETAILS:
		flist->priv->w_view = flist_table_new (flist);
		break;
	case VIEW_ICONS:
		flist->priv->w_view = flist_icon_list_new (flist);
		break;
	default:
		g_warning ("not yet supported");
		return;
	}

	flist->priv->view = view;
	gtk_option_menu_set_history (GTK_OPTION_MENU (flist->priv->type_combo),
				     view);
	flist_update_selected_count (flist, 0);

	if (flist->priv->w_view) {
		gtk_widget_show_all (flist->priv->w_view);
		gtk_container_add (GTK_CONTAINER (flist->priv->w_container),
				   flist->priv->w_view);
	}
}

static void 
flist_set_prop (BonoboPropertyBag *bag,
		const BonoboArg *arg,
		guint arg_id,
		CORBA_Environment *ev,
		gpointer user_data)
{
	FList *flist = FLIST (user_data);
	char *s;
	int i;

	switch (arg_id) {
	case PROP_ACCEPT_DIRS:
		flist->priv->accept_dirs = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
	case PROP_ENABLE_VFS:
		i = BONOBO_ARG_GET_BOOLEAN (arg);
		if (flist->priv->enable_vfs == i)
			return;
		flist->priv->enable_vfs = i;
		gtk_signal_emit (GTK_OBJECT (flist), flist_signal[SIGNAL_ENABLE_VFS_CHANGED], i);
		break;
	case PROP_MIME_TYPES:
		s = BONOBO_ARG_GET_STRING (arg);
		if (flist->priv->mime_types)
			g_strfreev (flist->priv->mime_types);
		flist->priv->mime_types = (s && strlen (s)) 
			? g_strsplit (s, ",", -1)
			: NULL;
		gtk_signal_emit (GTK_OBJECT (flist), flist_signal[SIGNAL_MIME_TYPES_CHANGED]);
		break;
	case PROP_MULTI_SELECT:
		i = BONOBO_ARG_GET_BOOLEAN (arg);
		if (flist->priv->multi_select == i)
			break;
		flist->priv->multi_select = i;
		gtk_signal_emit (GTK_OBJECT (flist), flist_signal[SIGNAL_MULTI_SELECT_CHANGED], i);
		break;
	case PROP_REQUESTED_URI:
		flist_request_uri (flist, BONOBO_ARG_GET_STRING (arg), FLIST_REQUEST_ABSOLUT);
		break;
	case PROP_VIEW:
		s = BONOBO_ARG_GET_STRING (arg);
		for (i = 0; i < VIEW_LAST; i++) {
			if (!g_strcasecmp (s, view_name[i])) {
				flist_set_view (flist, i);
				return;
			}
		}
		bonobo_exception_set (ev, ex_Bonobo_Property_InvalidValue);
		break;
	}
}

static void
flist_get_prop (BonoboPropertyBag *bag,
		BonoboArg *arg,
		guint arg_id,
		CORBA_Environment *ev,
		gpointer user_data)
{
	FList *flist = user_data;
	int i = 0;
	char *s;

	switch (arg_id) {
	case PROP_ACCEPT_DIRS:
		BONOBO_ARG_SET_BOOLEAN (arg, flist->priv->accept_dirs);
		break;
	case PROP_CURRENT_WORKING_URI:
		BONOBO_ARG_SET_STRING (arg, flist->priv->current_uri);
		break;
	case PROP_ENABLE_VFS:
		BONOBO_ARG_SET_BOOLEAN (arg, flist->priv->enable_vfs);
		break;
	case PROP_MIME_TYPES:
		s = flist->priv->mime_types
			? g_strjoinv (",", flist->priv->mime_types)
			: NULL;
		BONOBO_ARG_SET_STRING (arg, s);
		g_free (s);
		break;
	case PROP_MULTI_SELECT:
		BONOBO_ARG_SET_BOOLEAN (arg, flist->priv->multi_select);
		break;
	case PROP_REQUESTED_URI:
		BONOBO_ARG_SET_STRING (arg, flist->priv->requested_uri);
		break;
	case PROP_VIEW:
		BONOBO_ARG_SET_STRING (arg, view_name[flist->priv->view]);
		break;
	case PROP_SELECTED_URI_COUNT:
		switch (flist->priv->view) {
		case VIEW_DETAILS:
			i = flist_table_get_selected_count (flist->priv->w_view);
			break;
		case VIEW_ICONS:
			i = flist_icon_list_get_selected_count (flist->priv->w_view);
			break;
		default:
			g_assert_not_reached ();
			break;
		}
		BONOBO_ARG_SET_INT (arg, i);
		break;
	}
}

static void
flist_destroy (GtkObject *object)
{
	FList *flist;
	FListPrivate *priv;

	flist = FLIST (object);
	priv  = flist->priv;

	d(g_print ("flist_destroy()\n"));
	
	/* so we don't unref things that are us 
	   ie: other objects we get aggregated with (priv->es)
	*/
	/* fixme: unref other things */

	if (priv->pb)
		bonobo_object_unref (BONOBO_OBJECT (priv->pb));

	if (priv->type_control)
		bonobo_object_unref (priv->type_control);

	g_free (priv->current_uri);

	if (priv->mime_types)
		g_strfreev (priv->mime_types);
	
	g_free (priv->requested_uri);

	g_free (priv);
	flist->priv = NULL;

	parent_class->destroy (object);
}

static Bonobo_Control
impl_getTypeSelectorControl (PortableServer_Servant servant, 
			     CORBA_Environment *ev)
{
	FList *flist;
	flist = FLIST (BONOBO_X_SERVANT_GET_OBJECT (servant));

	return bonobo_object_dup_ref (BONOBO_OBJREF (flist->priv->type_control), ev);
}

static void
impl_activateSelectedURIs (PortableServer_Servant servant,
			   CORBA_Environment *ev)
{
	FList *flist;

	flist = FLIST (BONOBO_X_SERVANT_GET_OBJECT (servant));

	switch (flist->priv->view) {
	case VIEW_DETAILS:
		flist_table_activate_selected_uris (flist->priv->w_view);
		break;
	case VIEW_ICONS:
		flist_icon_list_activate_selected_uris (flist->priv->w_view);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}

static void
impl_requestURI (PortableServer_Servant servant,
		 const char *uri,
		 GNOME_FileSelector_List_RequestType rtype,
		 CORBA_Environment *ev)
{
	FList *flist;
	flist = FLIST (BONOBO_X_SERVANT_GET_OBJECT (servant));

	/* we could do this in an idle... */
	flist_request_uri (flist, uri, rtype);
}

static GNOME_FileSelector_List_URIList *
impl_getSelectedURIs (PortableServer_Servant servant, 
                      CORBA_Environment *ev)
{
        CORBA_sequence_CORBA_string *seq = NULL;
        FList *flist;

        flist = FLIST (BONOBO_X_SERVANT_GET_OBJECT (servant));

        switch (flist->priv->view) {
        case VIEW_DETAILS:
                seq = flist_table_get_selected_uris (flist->priv->w_view);
                break;
        case VIEW_ICONS:
                seq = flist_icon_list_get_selected_uris (flist->priv->w_view);
                break;
        default:
                g_assert_not_reached ();
                break;
        }

        return seq;
}

static void
flist_uri_changed (FList *flist, const char *uri)
{
	BonoboArg *arg;

	d(g_print ("(flist) uri changed: %s\n", uri));

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, uri);
	bonobo_property_bag_notify_listeners (flist->priv->pb,
					      prop_name[PROP_CURRENT_WORKING_URI], 
					      arg, NULL);
	bonobo_arg_release (arg);
}

static void
flist_uri_requested (FList *flist, const char *uri, FListRequestType rtype)
{
	BonoboArg *arg;

	d(g_print ("(flist) uri requested: %s (%d)\n", uri, rtype));

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, uri);
	bonobo_property_bag_notify_listeners (flist->priv->pb,
					      prop_name[PROP_REQUESTED_URI],
					      arg, NULL);
	bonobo_arg_release (arg);
}

/* FIXME: save this value so we don't emit unnecessary changes? */
void
flist_update_selected_count (FList *flist, int count)
{
	BonoboArg *arg;

	if (!flist->priv->pb)
		return;

	arg = bonobo_arg_new (BONOBO_ARG_INT);
	BONOBO_ARG_SET_INT (arg, count);
	bonobo_property_bag_notify_listeners (flist->priv->pb,
					      prop_name[PROP_SELECTED_URI_COUNT],
					      arg, NULL);
	bonobo_arg_release (arg);
}

static void
flist_class_init (GtkObjectClass *object_class)
{
	FListClass *flist_class;
	flist_class = FLIST_CLASS (object_class);

	parent_class = gtk_type_class (PARENT_TYPE);

	object_class->destroy = flist_destroy;

	flist_signal[SIGNAL_URI_CHANGED] =
		gtk_signal_new ("uri_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, uri_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	flist_signal[SIGNAL_URI_REQUESTED] =
		gtk_signal_new ("uri_requested",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, uri_requested),
				gtk_marshal_NONE__POINTER_INT,
				GTK_TYPE_NONE, 2, 
				GTK_TYPE_POINTER, GTK_TYPE_INT);

	flist_signal[SIGNAL_LOAD_CANCELLED] =
		gtk_signal_new ("load_cancelled",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, load_cancelled),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	flist_signal[SIGNAL_ENABLE_VFS_CHANGED] = 
		gtk_signal_new ("enable_vfs_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, enable_vfs_changed),
				gtk_marshal_NONE__BOOL,
				GTK_TYPE_NONE, 1, GTK_TYPE_BOOL);

	flist_signal[SIGNAL_MIME_TYPES_CHANGED] = 
		gtk_signal_new ("mime_types_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, mime_types_changed),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	flist_signal[SIGNAL_MULTI_SELECT_CHANGED] =
		gtk_signal_new ("multi_select_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (FListClass, multi_select_changed),
				gtk_marshal_NONE__BOOL,
				GTK_TYPE_NONE, 1, GTK_TYPE_BOOL);

	gtk_object_class_add_signals (object_class, flist_signal, SIGNAL_LAST);

	flist_class->uri_changed   = flist_uri_changed;
	flist_class->uri_requested = flist_uri_requested;

	flist_class->epv.getTypeSelectorControl   = impl_getTypeSelectorControl;
	flist_class->epv.activateSelectedURIs     = impl_activateSelectedURIs;
	flist_class->epv.getSelectedURIs          = impl_getSelectedURIs;
	flist_class->epv.cancelLoading            = impl_cancelLoading;
	flist_class->epv.requestURI               = impl_requestURI;


	if (!gnome_vfs_initialized ())
		gnome_vfs_init ();
}

static void
view_cb (GtkObject *w, gpointer data)
{
	flist_set_view (gtk_object_get_user_data (w),
			GPOINTER_TO_INT (data));
}

static void
append_view (FList *flist, GtkMenu *menu, int i)
{
	GtkWidget *item, *hbox;

	item = gtk_menu_item_new ();
	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);

	gtk_box_pack_start (GTK_BOX (hbox),
			    gnome_pixmap_new_from_file (view_icon[i]),
			    FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (hbox),
			    gtk_label_new (_(view_name[i])),
			    FALSE, FALSE, 0);

	gtk_object_set_user_data (GTK_OBJECT (item), flist);
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			    GTK_SIGNAL_FUNC (view_cb),
			    GINT_TO_POINTER (i));

	gtk_container_add (GTK_CONTAINER (item), hbox);

	gtk_menu_append (menu, item);
}

static void
create_type_combo (FList *flist)
{
	GtkWidget *menu;
	int i;

	/*
	 * Type selector widget
	 *
	 * just use an option menu for now.  what it should be is an
	 * option menu which doesn't actually show the text for the
	 * main view, and shows the state in the menu.
	 */

	menu = gtk_menu_new ();
	for (i = 0; i < VIEW_LAST; i++)
		append_view (flist, GTK_MENU (menu), i);
	gtk_widget_show_all (menu);

	flist->priv->type_combo = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (flist->priv->type_combo),
				  menu);

	gtk_widget_show_all (flist->priv->type_combo);

	flist->priv->type_control = BONOBO_OBJECT (bonobo_control_new (flist->priv->type_combo));
}

void
flist_uris_activated (FList *flist, CORBA_sequence_CORBA_string *seq)
{
	CORBA_any any;

	any._type = TC_CORBA_sequence_CORBA_string;
	any._value = seq;
	any._release = FALSE;

	bonobo_event_source_notify_listeners_full (flist->priv->es,
						   "GNOME/FileSelector/List",
						   "URIsActivated",
						   NULL, &any, NULL);
	CORBA_free (seq);
}

#define ADD_PROP(prop, prop_type, flags) \
bonobo_property_bag_add (pb, prop_name[prop], prop, prop_type, NULL, _(prop_desc[prop]), flags)

#define RW BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE
#define RO BONOBO_PROPERTY_READABLE

static void
add_properties (FList *flist, BonoboControl *control)
{
	BonoboPropertyBag *pb;

	pb = bonobo_property_bag_new (flist_get_prop, flist_set_prop, flist);

	ADD_PROP (PROP_ACCEPT_DIRS,         BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_CURRENT_WORKING_URI, BONOBO_ARG_STRING,  RO);
	ADD_PROP (PROP_ENABLE_VFS,          BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_MIME_TYPES,          BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_MULTI_SELECT,        BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_REQUESTED_URI,       BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_SELECTED_URI_COUNT,  BONOBO_ARG_INT,     RO);
	ADD_PROP (PROP_VIEW,                BONOBO_ARG_STRING,  RW);

	flist->priv->pb = pb;
	bonobo_control_set_properties (control, pb);
}

static void
flist_init (GtkObject *object)
{
	FList *flist = FLIST (object);
	BonoboControl *control;

	flist->priv = g_new0 (FListPrivate, 1);

	flist->priv->enable_vfs = TRUE;

	/*
	 * The notebook where we place the various file listing views
	 */
	flist->priv->w_container = gtk_event_box_new ();

	gtk_widget_show (GTK_WIDGET (flist->priv->w_container));
	/*
	 * Setup the directory listing
	 */
	create_type_combo (flist);

	/* we shouldn't set view until we have a dir and a combo */
	flist_set_view (flist, VIEW_DETAILS);

	gtk_widget_show_all (flist->priv->w_container);
	
	control = bonobo_control_new (flist->priv->w_container);
	bonobo_object_add_interface (BONOBO_OBJECT (flist), BONOBO_OBJECT (control));

	flist->priv->es = bonobo_event_source_new ();
	bonobo_object_add_interface (BONOBO_OBJECT (flist), BONOBO_OBJECT (flist->priv->es));

	add_properties (flist, control);
}

BONOBO_X_TYPE_FUNC_FULL(FList, GNOME_FileSelector_List, PARENT_TYPE, flist);
