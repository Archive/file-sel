/*
 * flist-util.h: some unix util functions
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_UTIL_H_
#define _FLIST_UTIL_H_

const char *flist_util_get_username  (int uid);
const char *flist_util_get_groupname (int gid);

#endif /* _FLIST_UTIL_H_ */
