/*
 * flist-dir.h: directory listing object
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_DIR_H_
#define _FLIST_DIR_H_

#include <libgnomevfs/gnome-vfs-types.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define FLIST_DIR_VFS_INFO_IS(i, f) (((i)->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE) \
				     && ((i)->type & (f)))
#define FLIST_DIR_VFS_INFO_IS_DIR(i) (FLIST_DIR_VFS_INFO_IS ((i), GNOME_VFS_FILE_TYPE_DIRECTORY))

typedef enum {
	FLIST_REQUEST_ABSOLUT,
	FLIST_REQUEST_RELATIVE,
	FLIST_REQUEST_TRY_RELATIVE
} FListRequestType;

typedef struct _FListDirPrivate FListDirPrivate;

typedef struct {
	FListDirPrivate *priv;

	char *uri;
} FListDir;

typedef struct {
	void (*directory_loading) (FListDir *dir, const char *uri, gpointer data);
	void (*files_read)        (FListDir *dir, int oldsize, 
				   int newfiles, gpointer data);
	void (*clear)             (FListDir *dir, int oldsize, gpointer data);
	void (*file_activated)    (FListDir *dir, const char *uri, gpointer data);
	void (*info_changed)      (FListDir *dir, int idx, gpointer data);
	void (*file_deleted)      (FListDir *dir, int idx, gpointer data);
	void (*report_error)      (FListDir *dir, const char *msg, gpointer data);
	void (*directory_done)    (FListDir *dir, gpointer data);
	void (*load_cancelled)    (FListDir *dir, gpointer data);
} FListDirView;

FListDir   *flist_dir_new             (FListDirView *view, gpointer data);
void        flist_dir_free            (FListDir *dir);

void        flist_dir_request         (FListDir *dir, int index);
void        flist_dir_request_uri     (FListDir *dir, const char *uri, FListRequestType rtype);
void        flist_dir_cancel          (FListDir *dir);

void        flist_dir_set_filter      (FListDir *dir, GnomeVFSDirectoryFilter *filter, gboolean free_filter);
void        flist_dir_refilter        (FListDir *dir);

void        flist_dir_rename          (FListDir *dir, int index, const char *new_name);
void        flist_dir_delete          (FListDir *dir, int index);

void        flist_dir_set_enable_vfs  (FListDir *dir, gboolean enable_vfs);
gboolean    flist_dir_get_enable_vfs  (FListDir *dir);

int                     flist_dir_get_size        (const FListDir *dir);
const GnomeVFSFileInfo *flist_dir_get_vfs_info    (const FListDir *dir, int index);
const GdkPixbuf        *flist_dir_get_pixbuf      (const FListDir *dir, int index);
const char             *flist_dir_get_description (const FListDir *dir, int index);
const char             *flist_dir_get_username    (const FListDir *dir, int index);
const char             *flist_dir_get_groupname   (const FListDir *dir, int index);
char                   *flist_dir_get_path        (const FListDir *dir, int index);

/* for debugging */
void      flist_dir_dump_infos         (const FListDir *dir);

#if 0 /* maybe some of these? */
void      flist_dir_load               (FListDir *dir);
void      flist_dir_refresh            (FListDir *dir);

GnomeVFSFileInfo *flist_dir_get_info (FListDir *dir, int idx);
#endif

#endif /* _FLIST_DIR_H_ */
