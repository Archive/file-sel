/*
 * flist-entry.c: an autocompleting entry control
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-entry.h"

#include "flist-dir.h"
#include "flist-completion-tree.h"

#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-property-bag.h>
#include <bonobo/bonobo-shlib-factory.h>

#include <eel/eel-vfs-extensions.h>

#include <gal/e-text/e-entry.h>

#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>

#include <libgnome/gnome-i18n.h>

#define FLIST_ENTRY_KEY "FListEntryKey"

#define FLIST_ENTRY(o) ((FListEntry *)gtk_object_get_data (GTK_OBJECT (o), FLIST_ENTRY_KEY))
#define SET_FLIST_ENTRY(o, e)        (gtk_object_set_data (GTK_OBJECT (o), FLIST_ENTRY_KEY, e))

#define TIMEOUT 150

#define d(x) x

enum {
	PROP_TEXT,
	PROP_CURRENT_WORKING_URI,
	PROP_URI,
	PROP_ENABLE_VFS
};

typedef enum {
	DIR_LOAD_NONE,
	DIR_LOAD_LOADING,
	DIR_LOAD_DONE,
#if 0
	DIR_LOAD_UNKNOWN,
	DIR_LOAD_ERROR
#endif
} DirLoadType;

#define DIR_LOAD_OK(d) (((d) == DIR_LOAD_LOADING) || ((d) == DIR_LOAD_DONE))

/* #define USE_TIMEOUT 1 */

typedef struct {
	/* entry things */
	GtkWidget         *entry;
	BonoboPropertyBag *pb;

	GNode             *completion_tree;
#ifdef USE_MATCH_HASH
	GHashTable        *match_hash;
	GHashTable        *old_match_hash;
#endif
	ECompletion       *completion;

	FListDir          *dir;
	guint              timeout_id;

	char *location;
	char *uri;

	char              *last_dirname_text;
	char              *last_dirname;
	const char        *txt;

	DirLoadType        dir_load_type;
	gboolean           match_all;

	int last_dirname_len;
	int len;
	int count;
} FListEntry;

static void
add_match (FListEntry *fentry, const GnomeVFSFileInfo *info)
{
	ECompletionMatch *match;
	char *menu_s, *match_s, *s;

#ifdef USE_MATCH_HASH
	if (fentry->old_match_hash) {
		match = g_hash_table_lookup (fentry->old_match_hash, info);
		if (match)
			return FALSE;
	}
#endif

	s = FLIST_DIR_VFS_INFO_IS_DIR (info) ? "/" : NULL;
	match_s = g_strconcat (fentry->last_dirname,      info->name, s, NULL);
	menu_s  = g_strconcat (fentry->last_dirname_text, info->name, s, NULL);

	match = e_completion_match_new (match_s, menu_s, strlen (menu_s) / (double)fentry->len);
	match->user_data = fentry;
	g_free (match_s);
	g_free (menu_s);

#ifdef USE_MATCH_HASH
	e_completion_match_ref (match);
	g_hash_table_insert (fentry->match_hash, info, match);
#endif

	e_completion_found_match (fentry->completion, match);
}

static gboolean
add_matches (GNode *node, gpointer data)
{
	const GnomeVFSFileInfo *info;

	info = flist_completion_tree_data (node);

	if (info)
		add_match (data, info);

	/* FIXME: stop after N matches? */
	
	return FALSE;
}

#if 0
/* this bad boy needs some soptimization */
static gint
search_timeout (FListEntry *fentry)
{
	ECompletionMatch *match;
	const GnomeVFSFileInfo *finfo;
	int i;
	char *s;

	g_return_val_if_fail (fentry->dir != NULL, FALSE);
	g_return_val_if_fail (fentry->last_dirname_text != NULL, FALSE);

	g_print ("timeout [%d, %d]: %s\n",
		 fentry->dir_load_type,
		 fentry->last_dirname_len,
		 fentry->last_dirname_text);

	/* FIXME: this shoudln't happen - we should kill the timeout
	 * if this is invalid */
	if (fentry->dir_load_type == DIR_LOAD_NONE) {
		g_message ("FIXME: bad directory");
		fentry->timeout_id = 0;
		return FALSE;
	}

	/* FIXME: only do N at a time per iteration */
	for (i = MIN (fentry->current + 25, fentry->count); fentry->current < i; fentry->current++) {
		finfo = flist_dir_get_vfs_info (fentry->dir, fentry->current);
		
		/*g_print ("\t%s\n", s);*/

		if (!fentry->match_all && 
		    strncmp (fentry->txt + fentry->last_dirname_len,
			     finfo->name,
			     fentry->len - fentry->last_dirname_len)) {
			continue;
		}
		
		fentry->nmatches++;
		/* FIXME: utf8, anyone? */
		match = e_completion_match_new (s, s,
						fentry->len / (double)strlen (s));
		match->user_data = fentry;
		e_completion_match_ref (match);
		e_completion_found_match (fentry->completion, match);

		g_free (s);
	}


	if (i == fentry->count) {
		fentry->timeout_id = 0;
		/* no matches :( */
		if (!fentry->nmatches)
			e_completion_cancel_search (fentry->completion);
		return FALSE;
	}
	
	return TRUE;
}
#else /* 0 */
static gint
search_timeout (FListEntry *fentry)
{
	if (fentry->match_all) {
		int i, s;
		s = flist_dir_get_size (fentry->dir);
		for (i = 0; i < s; i++)
			add_match (fentry, flist_dir_get_vfs_info (fentry->dir, i));
		if (i == 0)
			e_completion_cancel_search (fentry->completion);
	} else {
		GNode *matches;
		matches = flist_completion_tree_lookup (fentry->completion_tree, 
							fentry->txt + strlen (fentry->last_dirname_text));

		if (matches)
			g_node_traverse (matches, G_IN_ORDER, G_TRAVERSE_ALL, -1, add_matches, fentry);
		else
			e_completion_cancel_search (fentry->completion);
	}
	
	fentry->timeout_id = 0;
	return FALSE;
}
#endif /* 0 */

static void
remove_timeout (FListEntry *fentry)
{
	if (!fentry->timeout_id)
		return;

	gtk_timeout_remove (fentry->timeout_id);
	fentry->timeout_id = 0;
}

static void
start_timeout (FListEntry *fentry)
{
	remove_timeout (fentry);

	fentry->timeout_id = gtk_timeout_add (TIMEOUT, (GtkFunction)search_timeout, fentry);
}


static void
begin_completion (ECompletion *comp, const gchar *search_text, 
		  gint pos, gint limit, FListEntry *fentry)
{
	d(g_print ("begin_completion(%d, %s)\n", fentry->dir_load_type, search_text));

	if (fentry->dir_load_type == DIR_LOAD_NONE) {
		e_completion_cancel_search (comp);
		remove_timeout (fentry);
		return;
	}

	if (fentry->dir_load_type == DIR_LOAD_DONE)
		start_timeout (fentry);
}

static void
end_completion (ECompletion *comp, FListEntry *fentry)
{
	d(g_print ("end_completion()\n"));
	remove_timeout (fentry);
}

static void
cancel_completion (ECompletion *comp, FListEntry *fentry)
{
	d(g_print ("cancel_completion()\n"));
	remove_timeout (fentry);
}

static int
chop_last_slash (char *uri)
{
	char *last;

	last = strrchr (uri, '/');
	if (last) {
		last[1] = '\0';
		return last - uri + 1;
	}

	*uri = '\0';
	return 0;
}

static void
reset_completion (FListEntry *fentry)
{
	g_free (fentry->last_dirname);
	g_free (fentry->last_dirname_text);
	
	fentry->last_dirname = fentry->last_dirname_text = NULL;

	flist_dir_cancel (fentry->dir);
	fentry->dir_load_type = DIR_LOAD_NONE;
	
	if (e_completion_searching (fentry->completion))
		e_completion_cancel_search (fentry->completion);
}

static void
request_uri (FListEntry *fentry)
{
	BonoboArg *arg;
	char *s;

	g_print ("entry requesting: %s\n", fentry->uri);

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, fentry->uri);
	bonobo_property_bag_notify_listeners (fentry->pb, "URI", arg, NULL);
	bonobo_arg_release (arg);
}

static void
completion_cb (EEntry *entry, ECompletionMatch *match)
{
	FListEntry *fentry;
	fentry = match->user_data;
	/* changing text destroys the match! */
	e_entry_set_text (entry, match->match_text);
	request_uri (fentry);
}

static void
on_entry_activated (EEntry *eentry, FListEntry *fentry)
{
	request_uri (fentry);
}

static void
on_entry_changed (EEntry *eentry, FListEntry *fentry)
{
	BonoboArg *arg;
	const char *s;
	char *uri;
	gboolean searching;

	s = e_entry_get_text (eentry);	

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, s);
	bonobo_property_bag_notify_listeners (fentry->pb, "Text", arg, NULL);
	bonobo_arg_release (arg);

	g_free (fentry->uri);

	g_print ("current: %s\n", fentry->location);

	fentry->uri = (*s == '/' || *s == '~')
		? eel_make_uri_from_input (s)
		: eel_uri_make_full_from_relative (fentry->location, s);

	if (!*s || !fentry->uri) goto bad_uri;

	uri = g_strdup (fentry->uri);
	if (!chop_last_slash (uri)) {
		g_free (uri);
		goto bad_uri;
	}
	
	g_print ("last: %s\tthis: %s\n", fentry->last_dirname, uri);
	
	fentry->match_all = (s[strlen (s)-1] == '/');
	g_print ("matching all: %c %d\n", s[strlen (s)-1], fentry->match_all);	

	fentry->txt = s;
	fentry->len = strlen (s);
		
	/* FIXME: only clear when we have to.  some times we will be
	 * able to just go through the list of completions we have and
	 * remove the one or two that don't match.  doing this will
	 * like cut down on flicker and what not, man */
	searching = e_completion_searching (fentry->completion);
	
	if (fentry->last_dirname && !strcmp (fentry->last_dirname, uri)) {
		/* directory is the same */
		/* FIXME: find out like when we should restart the
		 * search and what not */
		g_free (uri);
		
		if (!DIR_LOAD_OK (fentry->dir_load_type))
			return;

		if (searching) {
			/* if (fentry->timeout_id) */
			e_completion_clear (fentry->completion);
			start_timeout (fentry);
		} else {
			e_completion_begin_search (fentry->completion,
						   fentry->txt,
						   0, 100);
		}
		return;
	}
	
	if (searching)
		e_completion_clear (fentry->completion);

	g_free (fentry->last_dirname);
	fentry->last_dirname = uri;
	
	g_free (fentry->last_dirname_text);
	fentry->last_dirname_text = g_strdup (s);
	fentry->last_dirname_len = chop_last_slash (fentry->last_dirname_text);
	
	g_print ("will match against: %s\n"
		 "looking up: %s\n\n",
		 fentry->last_dirname_text,
		 uri);
	
	fentry->dir_load_type = DIR_LOAD_NONE;
	flist_dir_request_uri (fentry->dir, uri, FLIST_REQUEST_ABSOLUT);
	return;

 bad_uri:
	reset_completion (fentry);
}

static void
entry_get_prop (BonoboPropertyBag *pb,
		BonoboArg         *arg,
		guint              arg_id,
		CORBA_Environment *ev,
		FListEntry        *entry)
{
	const char *s = NULL;

	switch (arg_id) {
	case PROP_TEXT:
		s = e_entry_get_text (E_ENTRY (entry->entry));
		break;
	case PROP_CURRENT_WORKING_URI:
		s = entry->location;
		break;
	case PROP_URI:
		s = entry->uri;
		break;
	case PROP_ENABLE_VFS:
		BONOBO_ARG_SET_BOOLEAN (
			arg, flist_dir_get_enable_vfs (entry->dir));
		return;
	}

	BONOBO_ARG_SET_STRING (arg, s);
}

static void
entry_set_prop (BonoboPropertyBag *pb,
		BonoboArg         *arg,
		guint              arg_id,
		CORBA_Environment *ev,
		FListEntry        *entry)
{
	char *s;
	int len;

	s = BONOBO_ARG_GET_STRING (arg);

	switch (arg_id) {
	case PROP_TEXT:
		e_entry_set_text (E_ENTRY (entry->entry), s);
		break;
	case PROP_CURRENT_WORKING_URI:
		len = strlen (s);

		g_free (entry->location);
		/* add a trailing / since the eel func wants it */
		entry->location = g_strconcat (s, 
					       s[len-1] == '/'
					       ? NULL
					       : "/", 
					       NULL);

		g_free (entry->uri);
		entry->uri = g_strdup (entry->location);

		reset_completion (entry);
		break;
	case PROP_ENABLE_VFS:
		flist_dir_set_enable_vfs (entry->dir,
					  BONOBO_ARG_GET_BOOLEAN (arg));
		break;
	}
}

static void
directory_loading (FListDir *dir, const char *uri, gpointer data)
{
	FListEntry *fentry;
	fentry = data;

	g_print ("directory_loading, searching: %d\n", 
		 e_completion_searching (fentry->completion));

	fentry->dir_load_type = DIR_LOAD_LOADING;

	fentry->count = 0;
	if (fentry->completion_tree)
		flist_completion_tree_free (fentry->completion_tree, NULL, NULL);

	fentry->completion_tree = flist_completion_tree_new ();
}

static void
files_read (FListDir *dir, int oldsize, int newfiles, gpointer data)
{
	const GnomeVFSFileInfo *info;
	FListEntry *fentry;
	int i;

	fentry = data;

	g_return_if_fail (fentry->dir_load_type == DIR_LOAD_LOADING);
	g_return_if_fail (fentry->count == oldsize);

	fentry->count += newfiles;
	for (i = oldsize; i < fentry->count; i++) {
		info = flist_dir_get_vfs_info (dir, i);
		flist_completion_tree_insert (fentry->completion_tree,
					      info->name, (gpointer)info);
	}

	g_print ("read %d files (%d)\n", newfiles, fentry->count);
}

static void
clear (FListDir *dir, int oldsize, gpointer data)
{
	FListEntry *fentry;
	fentry = data;
	
	if (!e_completion_searching (fentry->completion))
		return;

	e_completion_clear (fentry->completion);
}

static void
file_activated (FListDir *dir, const char *uri, gpointer data)
{
}

static void
info_changed (FListDir *dir, int idx, gpointer data)
{
}

static void
file_deleted (FListDir *dir, int idx, gpointer data)
{
}

static void
report_error (FListDir *dir, const char *msg, gpointer data)
{
	FListEntry *fentry;
	fentry = data;

	g_message ("%s", msg);

	fentry->dir_load_type = DIR_LOAD_NONE;

	if (e_completion_searching (fentry->completion))
		e_completion_cancel_search (fentry->completion);	
}

static void
directory_done (FListDir *dir, gpointer data)
{
	FListEntry *fentry;
	fentry = data;

	fentry->dir_load_type = DIR_LOAD_DONE;

	if (fentry->last_dirname_text) {
		if (e_completion_searching (fentry->completion))
			start_timeout (fentry);
		else
			e_completion_begin_search (fentry->completion,
						   fentry->txt,
						   0, 100);
	}
}	

static void
load_cancelled (FListDir *dir, gpointer data)
{
	FListEntry *fentry;
	fentry = data;

	fentry->dir_load_type = DIR_LOAD_NONE;
}

static void
free_fentry (GtkObject *o, gpointer data)
{
	FListEntry *fentry;

	fentry = data;

	g_message ("freeing FListEntry...");
	
	if (fentry->dir)
		flist_dir_free (fentry->dir);

	if (fentry->pb)
		bonobo_object_unref (BONOBO_OBJECT (fentry->pb));

	remove_timeout (fentry);

	memset (fentry, 0, sizeof (FListEntry));
	g_free (fentry);
}

static FListDirView flist_entry_view = {
	directory_loading,
	files_read,
	clear,
	file_activated,
	info_changed,
	file_deleted,
	report_error,
	directory_done,
	load_cancelled
};

BonoboObject *
file_selector_entry_new (void)
{
	BonoboControl *control;
	FListEntry    *fentry;

	fentry = g_new0 (FListEntry, 1);

	fentry->dir        = flist_dir_new (&flist_entry_view, fentry);
	fentry->entry      = e_entry_new ();
	fentry->completion = e_completion_new ();
	fentry->pb         = bonobo_property_bag_new ((BonoboPropertyGetFn)entry_get_prop, 
						      (BonoboPropertySetFn)entry_set_prop, 
						      fentry);
	gtk_widget_show (fentry->entry);

	e_entry_enable_completion_full (E_ENTRY (fentry->entry),
					fentry->completion, -1, 
					completion_cb);

	control = bonobo_control_new (fentry->entry);

	gtk_signal_connect (GTK_OBJECT (control), "destroy",
			    GTK_SIGNAL_FUNC (free_fentry), fentry);

	gtk_signal_connect (GTK_OBJECT (fentry->completion),
			    "begin_completion",
			    GTK_SIGNAL_FUNC (begin_completion),
			    fentry);

	gtk_signal_connect (GTK_OBJECT (fentry->completion),
			    "end_completion",
			    GTK_SIGNAL_FUNC (end_completion),
			    fentry);

	gtk_signal_connect (GTK_OBJECT (fentry->completion),
			    "cancel_completion",
			    GTK_SIGNAL_FUNC (cancel_completion),
			    fentry);

	gtk_signal_connect (GTK_OBJECT (fentry->entry), "changed",
			    GTK_SIGNAL_FUNC (on_entry_changed),
			    fentry);
	
	gtk_signal_connect (GTK_OBJECT (fentry->entry), "activate",
			    GTK_SIGNAL_FUNC (on_entry_activated),
			    fentry);

	bonobo_property_bag_add (fentry->pb, "Text", PROP_TEXT,
				 BONOBO_ARG_STRING, NULL,
				 _("Text in the entry"), 0);

	bonobo_property_bag_add (fentry->pb, "CurrentWorkingURI", 
				 PROP_CURRENT_WORKING_URI,
				 BONOBO_ARG_STRING, NULL,
				 _("Current Directory"), 0);

	bonobo_property_bag_add (fentry->pb, "URI", PROP_TEXT,
				 BONOBO_ARG_STRING, NULL,
				 _("Complete URI in the entry"), 
				 BONOBO_PROPERTY_READABLE);

	bonobo_property_bag_add (fentry->pb, "EnableVFS", PROP_ENABLE_VFS,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Enable gnome-vfs aware URIs"), 0);

	bonobo_control_set_properties (control, fentry->pb);

	return BONOBO_OBJECT (control);
}
