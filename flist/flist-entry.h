/*
 * flist-entry.h: an autocompleting entry control
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_ENTRY_H_
#define _FLIST_ENTRY_H_

#include <bonobo/bonobo-object.h>
	
BonoboObject *file_selector_entry_new (void);

#endif /* _FLIST_ENTRY_H_ */
