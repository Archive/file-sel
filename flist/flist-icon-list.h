/*
 * flist-icon-list.h: list view for the flist
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_ICON_LIST_H_
#define _FLIST_ICON_LIST_H_

#include "flist.h"
#include <gtk/gtkwidget.h>

GtkWidget *flist_icon_list_new (FList *flist);

int flist_icon_list_get_selected_count (GtkWidget *w);

CORBA_sequence_CORBA_string *flist_icon_list_get_selected_uris (GtkWidget *w);

void flist_icon_list_activate_selected_uris (GtkWidget *w);

#endif /* _FLIST_ICON_LIST_H_ */

