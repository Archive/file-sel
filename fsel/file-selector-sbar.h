/*
 * Author:
 *  Lutz M�ller <urc8@rz.uni-karlsruhe.de>
 */

#ifndef _FILE_SELECTOR_SBAR_H_
#define _FILE_SELECTOR_SBAR_H_

#include <gal/shortcut-bar/e-shortcut-bar.h>
#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define FILE_SELECTOR_TYPE_SBAR         (file_selector_sbar_get_type ())
#define FILE_SELECTOR_SBAR(obj)         (GTK_CHECK_CAST (obj, FILE_SELECTOR_TYPE_SBAR, FileSelectorSBar))
#define FILE_SELECTOR_SBAR_CLASS(klass) (GTK_CHECK_CLASS_CAST (klass, FILE_SELECTOR_TYPE_SBAR, FileSelectorSBarClass))
#define FILE_SELECTOR_IS_SBAR(obj)      (GTK_CHECK_TYPE (obj, FILE_SELECTOR_TYPE_SBAR))

typedef struct _FileSelectorSBar        FileSelectorSBar;
typedef struct _FileSelectorSBarPrivate FileSelectorSBarPrivate;
typedef struct _FileSelectorSBarClass   FileSelectorSBarClass;

struct _FileSelectorSBar
{
	EShortcutBar parent;

	FileSelectorSBarPrivate *priv;
};

struct _FileSelectorSBarClass
{
	EShortcutBarClass parent_class;

	void (*url_selected) (FileSelectorSBar *sbar, const char *url);
};

GtkType    file_selector_sbar_get_type (void);
GtkWidget *file_selector_sbar_new      (void);

END_GNOME_DECLS

#endif /* _FILE_SELECTOR_SBAR_H_ */
