/*
 * file-selector.c: implementation of GNOME::FileSelector
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *    Miguel de Icaza  <miguel@ximian.com>
 *
 * Copyright 2000, 2001 Ximian, Inc.
 */

#include <config.h>

#include "file-selector.h"

#include "widgets/flist-mime-icon.h"
#include "widgets/property-bag-item-handler.h"

#include "idl/GNOME_FileSelector_List.h"

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-item-handler.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo/bonobo-widget.h>

#include <gal/e-text/e-completion.h>
#include <gal/e-text/e-entry.h>
#include <gal/util/e-util.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include <glade/glade.h>

#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtksignal.h>

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>

#include <libgnomeui/gnome-pixmap.h>
#include <libgnomeui/gnome-uidefs.h>

#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#include "file-selector-sbar.h"

#define d(x)

enum {
	PROP_ACCEPT_DIRS,
	PROP_APPLICATION,
	PROP_DEFAULT_FILE_NAME,
	PROP_DEFAULT_LOCATION,
	PROP_DEFAULT_VIEW,
	PROP_ENABLE_VFS,
	PROP_MIME_TYPES,
	PROP_MIME_TYPES_SEQUENCE,
	PROP_MULTIPLE_SELECTION,
	PROP_REQUESTED_URI,
	PROP_SAVE_MODE,
	PROP_LAST
};

static const char *prop_name[] = {
	"AcceptDirectories",
	"Application",
	"DefaultFileName",
	"DefaultLocation",
	"DefaultView",
	"EnableVFS",
	"MimeTypes",
	"MimeTypesSequence",
	"MultipleSelection",
	"RequestedURI",
	"SaveMode",
};

static const char *prop_desc[] = {
	N_("Calling application can accept directories"),
	N_("Name of calling application"),
	N_("URI currently displayed in the file list"),
	N_("File name to default to when changing directories"),
	N_("Directory to start out in."),
	N_("Default view mode"),
	N_("Restrict files to local file paths only"),
	N_("List of mime types calling application can accept"),
	N_("List of mime types calling application can accept"),
	N_("Calling application can accept multiple URIs"),
	N_("Uri to load"),
	N_("Dialog is in save file mode"),
};

typedef Bonobo_EventSource_ListenerId ListenerId;

typedef struct {
	Bonobo_EventSource es;
	ListenerId         id;
} EventKit;

typedef struct {
	BonoboPropertyBag *properties;

	BonoboListener *flist_listener;
	BonoboListener *entry_listener;

	EventKit flist_kit[2];	
	EventKit entry_kit;

	BonoboEventSource *event_source;

	GnomeVFSURI       *current_uri;
	GnomeVFSURI       *requested_uri;

	GnomeVFSURI       *home_uri;
	GnomeVFSURI       *desktop_uri;
	GnomeVFSURI       *docs_uri;
	
	GtkWidget         *dialog;

	GNOME_FileSelector_List flist;

	GtkWidget *path_tree;
	GtkWidget *back;
	GtkWidget *up;
	GtkWidget *mkdir;
	GtkWidget *view_container;	

	GtkWidget *shortcut_bar;
	GtkWidget *file_list;

	GtkWidget *name_entry;
	GtkWidget *type_combo;

	GtkWidget *action_button;
	GtkWidget *cancel_button;

	GList *parent_uris;
	GList *favorite_uris;
	GList *recent_uris;

	char **mime_names;
	char **mime_types;

	gboolean last_was_flist;

	gboolean accept_dirs;
	gboolean save_mode;
	gboolean multiple_selection;

	char *app_name;

	GtkWidget *action_label;

	char *default_name;
	char *default_dir;
	char *default_view;

	gboolean entry_is_active;
	gboolean flist_is_active;
} FileSelector;

/*
 * Prototypes required because libglade will call us back, so they can not
 * be made static.
 */
GtkWidget *fsel_custom_control_new (char *name, gchar *moniker, gchar *string2, gint int1, gint int2);

GtkWidget *
fsel_custom_control_new (char *name, gchar *moniker, gchar *string2, gint int1, gint int2)
{
	g_return_val_if_fail (moniker && strlen (moniker), NULL);

	return bonobo_widget_new_control (moniker, CORBA_OBJECT_NIL);
}

static void
filter_item_cb (GtkWidget *w, const char *mime_type)
{
	FileSelector *fs = gtk_object_get_user_data (GTK_OBJECT (w));

	g_print ("setting filter: %s\n", mime_type ? mime_type : _("All Files"));

	bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
				    prop_name[PROP_MIME_TYPES],
				    mime_type,
				    NULL);
}

static void
add_mime_type (GtkMenu *menu, const char *description, const char *mime_type)
{
	GtkWidget *item;	
	const char *mime_desc = NULL;

	if (description && strlen (description))
		mime_desc = description;
	else if (mime_type && strlen (mime_type))
		mime_desc = gnome_vfs_mime_get_description (mime_type);
	else
		mime_desc = _("All Files");
	
	if (!mime_desc)
		mime_desc = _("Unknown Files");

	item = gtk_menu_item_new_with_label (mime_desc);
	gtk_object_set_user_data (GTK_OBJECT (item),
				  gtk_object_get_user_data (GTK_OBJECT (menu)));
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			    GTK_SIGNAL_FUNC (filter_item_cb),
			    (char *)mime_type);

	gtk_widget_show (item);
	gtk_menu_append (menu, item);
}

static void
fixup_type_combo (FileSelector *fs)
{
	GtkWidget *menu;
	int i;

	menu = gtk_menu_new ();
	gtk_object_set_user_data (GTK_OBJECT (menu), fs);
	
	if (fs->mime_types) {
		for (i = 0; fs->mime_names[i]; i++) {
			add_mime_type (GTK_MENU (menu),
				       fs->mime_names[i],
				       fs->mime_types[i]);
		}
	}
	add_mime_type (GTK_MENU (menu), NULL, NULL);

	gtk_widget_show (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (fs->type_combo),
				  menu);
}

static void
fixup_mime_types (FileSelector *fs, int n)
{
	char *s;
	int i;

	g_free (fs->mime_types);

	if (n < 0) {
		for (i=0; fs->mime_names[i]; i++)
			;
		n = i;
	}

	fs->mime_types = g_new (char *, n + 1);

	for (i = 0; i < n; i++) {
		s = strchr (fs->mime_names[i], ':');
		if (!s) {
			fs->mime_types[i] = NULL;
			continue;
		}
		*s = '\0';
		fs->mime_types[i] = s+1;
#if 0
		g_print ("name: %s\ttypes: %s\n", 
			 fs->mime_names[i], 
			 fs->mime_types[i]);
#endif
	}

	fs->mime_types[i] = NULL;

	fixup_type_combo (fs);
}

static void
update_action_enabled (FileSelector *fs)
{
	gtk_widget_set_sensitive (
		fs->action_button,
		(!fs->save_mode && fs->accept_dirs) ||
		(fs->last_was_flist && fs->flist_is_active) ||
		(fs->entry_is_active));
}

static void 
fs_set_prop (BonoboPropertyBag *bag,
	     const BonoboArg *arg,
	     guint arg_id,
	     CORBA_Environment *ev,
	     gpointer user_data)
{
	FileSelector *fs;
	int i;
	gboolean b;

	fs = user_data;

	switch (arg_id) {
	case PROP_ACCEPT_DIRS:
		fs->accept_dirs = BONOBO_ARG_GET_BOOLEAN (arg);
		if (!fs->save_mode)
			bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
						    prop_name[PROP_ACCEPT_DIRS],
						    fs->accept_dirs, NULL);
		update_action_enabled (fs);
		break;
	case PROP_APPLICATION:
		g_free (fs->app_name);
		fs->app_name = g_strdup (BONOBO_ARG_GET_STRING (arg));
		break;
	case PROP_DEFAULT_FILE_NAME:
		g_free (fs->default_name);
		fs->default_name = g_strdup (BONOBO_ARG_GET_STRING (arg));
		/* FIXME: do something */
		break;
	case PROP_DEFAULT_LOCATION:
		g_free (fs->default_dir);
		fs->default_dir = g_strdup (BONOBO_ARG_GET_STRING (arg));
		break;
	case PROP_DEFAULT_VIEW:
		g_free (fs->default_view);
		fs->default_view = g_strdup (BONOBO_ARG_GET_STRING (arg));
		/* FIXME: do something */
		break;
	case PROP_ENABLE_VFS:
		bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
					    prop_name[PROP_ENABLE_VFS],
					    BONOBO_ARG_GET_BOOLEAN (arg),
					    NULL);
		break;
	case PROP_MIME_TYPES:
		if (fs->mime_names)
			g_strfreev (fs->mime_names);

		fs->mime_names = g_strsplit (BONOBO_ARG_GET_STRING (arg),
					     "|", -1);

		fixup_mime_types (fs, -1);
		break;
	case PROP_MIME_TYPES_SEQUENCE: {
		CORBA_sequence_CORBA_string *seq;

		if (fs->mime_names)
			g_strfreev (fs->mime_names);

		seq = arg->_value;
		fs->mime_names = g_new (char *, seq->_length + 1);
		for (i = 0; i < seq->_length; i++)
			fs->mime_names[i] = g_strdup (seq->_buffer[i]);
		fs->mime_names[i] = NULL;
		fixup_mime_types (fs, seq->_length);
		break;
	}
	case PROP_MULTIPLE_SELECTION:
		fs->multiple_selection = BONOBO_ARG_GET_BOOLEAN (arg);
		if (!fs->save_mode)
			bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
						    prop_name[PROP_MULTIPLE_SELECTION],
						    fs->multiple_selection, NULL);
		update_action_enabled (fs);
		break;
	case PROP_REQUESTED_URI:
		bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
					    prop_name[PROP_REQUESTED_URI],
					    BONOBO_ARG_GET_STRING (arg),
					    NULL);
		break;
	case PROP_SAVE_MODE:
		b = BONOBO_ARG_GET_BOOLEAN (arg);
		if (b == fs->save_mode)
			break;

		fs->save_mode = b;
		/* FIXME: i don't think AcceptDirs matters any more*/
		bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
					    prop_name[PROP_MULTIPLE_SELECTION], 
					    b ? FALSE : fs->multiple_selection,
					    prop_name[PROP_ACCEPT_DIRS],
					    b ? FALSE : fs->accept_dirs,
					    NULL);
		
		/* FIXME: check directory selected property */

		update_action_enabled (fs);
		gtk_label_set_text (GTK_LABEL (fs->action_label),
				    b ? _("Save") : _("Open"));
		break;
	default:
		/* until bonobo sets this exception */
		if (arg_id >= 0 && arg_id < PROP_LAST)
			bonobo_exception_set (ev, ex_Bonobo_Property_ReadOnlyProperty);
		else
			g_assert_not_reached ();
		break;
	}
}

static void
fs_get_prop (BonoboPropertyBag *bag,
	     BonoboArg *arg,
	     guint arg_id,
	     CORBA_Environment *ev,
	     gpointer user_data)
{
	FileSelector *fs;
	gboolean b;
	char *s;

	fs = user_data;

	switch (arg_id) {
	case PROP_ACCEPT_DIRS:
		if (fs->save_mode)
			b = FALSE;
		else
			bonobo_widget_get_property (BONOBO_WIDGET (fs->file_list),
						    prop_name[PROP_ACCEPT_DIRS],
						    &b, NULL);
		BONOBO_ARG_SET_BOOLEAN (arg, b);
		break;
	case PROP_APPLICATION:
		BONOBO_ARG_SET_STRING (arg, fs->app_name);
		break;
	case PROP_DEFAULT_FILE_NAME:
		BONOBO_ARG_SET_STRING (arg, fs->default_name);
		break;
	case PROP_DEFAULT_LOCATION:
		BONOBO_ARG_SET_STRING (arg, fs->default_dir);
		break;
	case PROP_DEFAULT_VIEW:
		BONOBO_ARG_SET_STRING (arg, fs->default_view);
		break;
	case PROP_ENABLE_VFS:
		bonobo_widget_get_property (BONOBO_WIDGET (fs->file_list),
					    prop_name[PROP_ENABLE_VFS],
					    &b, NULL);
		BONOBO_ARG_SET_BOOLEAN (arg, b);
		break;
	case PROP_MIME_TYPES:
#if 0
		s = fs->mime_types
			? g_strjoinv (":", fs->mime_types)
			: NULL;
		BONOBO_ARG_SET_STRING (arg, s);
		g_free (s);
#else
		BONOBO_ARG_SET_STRING (arg, "");
#endif
		break;
	case PROP_MIME_TYPES_SEQUENCE: {
		CORBA_sequence_CORBA_string *seq;
		int len;

		len = fs->mime_types ? sizeof (fs->mime_types) / sizeof (char *) : 0;
		g_print ("we have %d mime types\n", len);

		seq = CORBA_sequence_CORBA_string__alloc ();
		seq->_length = len;
		if (len) {
			int i;
			seq->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
			for (i = 0; i < len; i++)
				seq->_buffer[i] = CORBA_string_dup (fs->mime_types[i]);
		}
		arg->_value = seq;
		break;
	}
	case PROP_MULTIPLE_SELECTION:
		if (fs->save_mode)
			b = FALSE;
		else
			bonobo_widget_get_property (BONOBO_WIDGET (fs->file_list),
						    prop_name[PROP_MULTIPLE_SELECTION],
						    &b, NULL);
		BONOBO_ARG_SET_BOOLEAN (arg, b);
		break;
	case PROP_REQUESTED_URI:
		bonobo_widget_get_property (BONOBO_WIDGET (fs->file_list),
					    prop_name[PROP_REQUESTED_URI],
					    &s, NULL);
		BONOBO_ARG_SET_STRING (arg, s);
		break;
	case PROP_SAVE_MODE:
		BONOBO_ARG_SET_BOOLEAN (arg, fs->save_mode);
		break;
	default:
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		break;
	}
}

static void
file_selector_request_uri (FileSelector *fs, const char *uri, 
			   GNOME_FileSelector_List_RequestType rtype)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	GNOME_FileSelector_List_requestURI (fs->flist, uri, rtype, &ev);
	CORBA_exception_free (&ev);
}

static void
menu_activate_uri (GtkWidget *w, GnomeVFSURI *uri)
{
	FileSelector *fs;	
	char *uri_str;

	fs = gtk_object_get_user_data (GTK_OBJECT (w));
	uri_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
	g_print ("menu: got: %s\n", uri_str);

	file_selector_request_uri (fs, uri_str, GNOME_FileSelector_List_RequestAbsolute);
	
	g_free (uri_str);
}

static void
add_uri_to_menu_full (GtkMenu *menu, const char *label, GnomeVFSURI *uri)
{
	GtkWidget *hbox;
	GtkWidget *w;
	char *path, *file;

	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);

	path = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
	file = flist_mime_icon_get_file (path, GNOME_VFS_FILE_TYPE_DIRECTORY, NULL);
	g_free (path);

	if (file) {
		w = gnome_pixmap_new_from_file_at_size (file, 20, 20);
		gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
		g_free (file);
	}

	w = gtk_label_new (label);
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);

	gtk_widget_show_all (hbox);

	w = gtk_menu_item_new ();
	gtk_container_add (GTK_CONTAINER (w), hbox);

	gtk_object_set_user_data (GTK_OBJECT (w),
				  gtk_object_get_user_data (GTK_OBJECT (menu)));
	gtk_signal_connect (GTK_OBJECT (w), "activate",
			    GTK_SIGNAL_FUNC (menu_activate_uri),
			    uri);
	gtk_menu_append (menu, w);	
}

/* todo: get a pixmap based on the uri or something */
static void
add_uri_to_menu (GnomeVFSURI *uri, GtkMenu *menu)
{
	char *name;

	name = gnome_vfs_uri_extract_short_name (uri);
	add_uri_to_menu_full (menu, name, uri);
	g_free (name);
}

static void
add_dummy_to_menu (GtkWidget *w, const char *label)
{
	GtkWidget *menuitem;
	
	menuitem = label 
		? gtk_menu_item_new_with_label (label) 
		: gtk_menu_item_new ();
	gtk_widget_set_sensitive (menuitem, FALSE);
	gtk_widget_show (menuitem);
	gtk_menu_append (GTK_MENU (w), menuitem);
}

static void
free_uri_list (GList **list)
{
	gnome_vfs_uri_list_unref (*list);
	g_list_free (*list);
	*list = NULL;
}

static void
update_path_tree (FileSelector *fs, const char *uri)
{
	GnomeVFSURI *vfs_uri;
	GtkWidget *menu;

	free_uri_list (&fs->parent_uris);

	gnome_vfs_uri_ref (fs->current_uri);
	vfs_uri = fs->current_uri;

	do {
		fs->parent_uris = g_list_append (fs->parent_uris,
						       vfs_uri);
		vfs_uri = gnome_vfs_uri_get_parent (vfs_uri);
	} while (vfs_uri);

	menu = gtk_menu_new ();
	gtk_object_set_user_data (GTK_OBJECT (menu), fs);
	g_list_foreach (fs->parent_uris, (GFunc)add_uri_to_menu,  menu);

	add_dummy_to_menu (menu, NULL);
	/* these need custom names... */
	add_uri_to_menu_full (GTK_MENU (menu), _("Desktop"),   fs->desktop_uri);
	add_uri_to_menu_full (GTK_MENU (menu), _("Documents"), fs->docs_uri);
	add_uri_to_menu_full (GTK_MENU (menu), _("Home"),      fs->home_uri);

	add_dummy_to_menu (menu, NULL);
	add_dummy_to_menu (menu, _("Favorites"));
	g_list_foreach (fs->favorite_uris, (GFunc)add_uri_to_menu, menu);

	add_dummy_to_menu (menu, NULL);
	add_dummy_to_menu (menu, _("Recent"));
	g_list_foreach (fs->recent_uris, (GFunc)add_uri_to_menu, menu);

	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (fs->path_tree), menu);
}

static CORBA_sequence_CORBA_string *
string_seq_from_string (char *s)
{
	CORBA_sequence_CORBA_string *seq;

	seq = CORBA_sequence_CORBA_string__alloc ();
	seq->_length = 1;
	seq->_release = TRUE;
	
	seq->_buffer = CORBA_sequence_CORBA_string_allocbuf (seq->_length);
	seq->_buffer[0] = s;

	return seq;
}

static void
activate_uris (FileSelector *fs, CORBA_sequence_CORBA_string *inseq)
{
	CORBA_sequence_CORBA_string *seq;
	char *s;
	CORBA_any any;
	
	g_print ("last_was_flist = %d\n", fs->last_was_flist);	

	if (inseq) {
		seq = inseq;
	} else if (fs->last_was_flist && fs->flist_is_active) {
		CORBA_Environment ev;
		
		CORBA_exception_init (&ev);
		seq = GNOME_FileSelector_List_getSelectedURIs (fs->flist, &ev);
		CORBA_exception_free (&ev);

	} else if (!fs->last_was_flist && fs->entry_is_active) {
		bonobo_widget_get_property (BONOBO_WIDGET (fs->name_entry),
					    "URI", &s, NULL);
		bonobo_widget_set_property (BONOBO_WIDGET (fs->name_entry),
					    "Text", "", NULL);
		seq = string_seq_from_string (s);

	} else if (!fs->save_mode && fs->accept_dirs) {
		s = gnome_vfs_uri_to_string (fs->current_uri, 
					     GNOME_VFS_URI_HIDE_NONE);
		seq = string_seq_from_string (s);
	} else {
		g_message ("nothing to activate");
		return;
	}

	/* FIXME: they may have accept_dirs off, and have selected
	 * only directories.  pop up an error dialog or something */
	g_return_if_fail (seq->_length > 0);

	any._type    = TC_CORBA_sequence_CORBA_string;
	any._value   = seq;
	any._release = FALSE;

	bonobo_event_source_notify_listeners_full (fs->event_source,
						   "GNOME/FileSelector/Control",
						   "ButtonClicked",
						   "Action",
						   &any, NULL);
	if (!inseq)
		CORBA_free (seq);
}

static void
on_action_clicked (GtkWidget *w, FileSelector *fs)
{
	char *s;

	if (fs->last_was_flist && fs->flist_is_active) {
		GNOME_FileSelector_List_activateSelectedURIs (
			fs->flist, NULL);
	} else if (!fs->last_was_flist && fs->entry_is_active) {
		bonobo_widget_get_property (BONOBO_WIDGET (fs->name_entry),
					    "URI", &s, NULL);
		g_print ("\n\nURI: %s\n\n", s);
		file_selector_request_uri (fs, s, GNOME_FileSelector_List_RequestAbsolute);
		g_free (s);
	} else if (!fs->save_mode && fs->accept_dirs) {
		activate_uris (fs, NULL);
	}
}

static void
flist_listener_cb (BonoboListener *listener,
		   char *event_name,
		   CORBA_any *any,
		   CORBA_Environment *ev,
		   FileSelector *fs)
{
	char *uri_str;
	char *type, *subtype;

	g_return_if_fail (fs != NULL);

	type    = bonobo_event_type (event_name);
	subtype = bonobo_event_subtype (event_name);

	if (!strcmp (type, "Bonobo/Property:change")) {
		gboolean proxy_event = FALSE;

		if (!strcmp (subtype, "CurrentWorkingURI")) {
			uri_str = BONOBO_ARG_GET_STRING (any);

			g_print ("URI is now: %s\n", uri_str);

			if (fs->current_uri)
				gnome_vfs_uri_unref (fs->current_uri);
			
			fs->current_uri = gnome_vfs_uri_new (uri_str);
			gtk_widget_set_sensitive (fs->up, 
						  gnome_vfs_uri_has_parent (fs->current_uri));
			
			update_path_tree (fs, uri_str);
			bonobo_widget_set_property (BONOBO_WIDGET (fs->name_entry),
						    "CurrentWorkingURI", uri_str,
						    "Text", fs->default_name,
						    NULL);
		} else if (!strcmp (subtype, "SelectedURICount")) {
			fs->flist_is_active = BONOBO_ARG_GET_INT (any) > 0;
			update_action_enabled (fs);
		} else if (!strcmp (subtype, prop_name[PROP_ACCEPT_DIRS])) {
			fs->accept_dirs = BONOBO_ARG_GET_BOOLEAN (any);
			proxy_event = TRUE;
		} else if (!strcmp (subtype, prop_name[PROP_MULTIPLE_SELECTION]) ||
			   !strcmp (subtype, prop_name[PROP_ENABLE_VFS]) ||
			   !strcmp (subtype, prop_name[PROP_REQUESTED_URI]))
			proxy_event = TRUE;

		if (proxy_event) {
			g_message ("proxying: %s", subtype);
			bonobo_property_bag_notify_listeners (
				fs->properties,
				subtype, any, NULL);
		}
		
	} else if (!strcmp (type, "GNOME/FileSelector/List:URIsActivated")) {
		activate_uris (fs, fs->last_was_flist ? any->_value : NULL);
	} else if (!strcmp (type, "GNOME/FileSelector/List:Error")) {
		GtkWidget *d;

		d = gnome_error_dialog (BONOBO_ARG_GET_STRING (any));
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
	} else {
		g_warning ("unknown event: %s\n", event_name);
	}

	g_free (type);
	g_free (subtype);
}

static void
entry_listener_cb (BonoboListener *listener,
	     char *event_name,
	     CORBA_any *any,
	     CORBA_Environment *ev,
	     FileSelector *fs)
{
	char *type, *subtype;
	char *current_uri = NULL;

	g_return_if_fail (fs != NULL);

	type    = bonobo_event_type (event_name);
	subtype = bonobo_event_subtype (event_name);

	if (strcmp (type, "Bonobo/Property:change"))
		goto elcb_free_types;

	if (!strcmp (subtype, "Text")) {
		fs->entry_is_active = strlen (BONOBO_ARG_GET_STRING (any)) > 0;
		update_action_enabled (fs);
	} else if (!strcmp (subtype, "URI")) {
		const char *uri;

		uri = BONOBO_ARG_GET_STRING (any);

		if (fs->current_uri) {
			current_uri = gnome_vfs_uri_to_string (fs->current_uri, GNOME_VFS_URI_HIDE_NONE);
			if (!strcmp (uri, current_uri)) {
				bonobo_widget_set_property (BONOBO_WIDGET (fs->name_entry),
							    "Text", fs->default_name,
							    NULL);
				goto elcb_free_types;
				
			}
		}
		file_selector_request_uri (fs, uri,
					   GNOME_FileSelector_List_RequestAbsolute);
	}

 elcb_free_types:
	g_free (current_uri);
	g_free (subtype);
	g_free (type);
}

/* this can't be a custom widget since we need to have a handle to the
 * flist */
static void
create_view_combo (GNOME_FileSelector_List flist, GladeXML *ui)
{
	GtkWidget *w, *w2;
	Bonobo_Control control;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	control = GNOME_FileSelector_List_getTypeSelectorControl (flist, &ev);
	CORBA_exception_free (&ev);

	w = glade_xml_get_widget (ui, "flist-view");
	w2 = bonobo_widget_new_control_from_objref (control, CORBA_OBJECT_NIL);
	gtk_widget_show (w2);
	gtk_container_add (GTK_CONTAINER (w), w2);
}

static void
on_up_clicked (GtkWidget *w, FileSelector *fs)
{
#if 1
	file_selector_request_uri (fs, "..", GNOME_FileSelector_List_RequestRelative);
#else
	GnomeVFSURI *uri;
	
	char *uri_str;

	g_return_if_fail (gnome_vfs_uri_has_parent (fs->current_uri));

	uri = gnome_vfs_uri_get_parent (fs->current_uri);
	uri_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
	gnome_vfs_uri_unref (uri);
	
	bonobo_widget_set_property (BONOBO_WIDGET (fs->file_list),
				    prop_name[PROP_REQUESTED_URI],
				    uri_str, NULL);

	g_free (uri_str);
#endif
}

static void
on_file_selector_sbar_url_selected (FileSelectorSBar *sbar, const gchar *url, 
				    FileSelector *fs)
{
	char *uri;

	file_selector_request_uri (fs, url, GNOME_FileSelector_List_RequestAbsolute);
}

static void
on_cancel_clicked (GtkWidget *w, FileSelector *fs)
{
	BonoboArg *arg;
	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, "");
	bonobo_event_source_notify_listeners_full (fs->event_source,
						   "GNOME/FileSelector/Control",
						   "ButtonClicked",
						   "Cancel", arg, NULL);
	bonobo_arg_release (arg);
}

/* this is a pretty gross hack. */
static void
on_focus_child (GtkContainer *container, GtkWidget *widget, FileSelector *fs)
{
	GtkWidget *child, *grandchild;
	d(char *s);

	d(g_print ("on_focus_child(%p)   { %d, %p }\n", widget, fs->last_was_flist, fs->file_list));

	if (!widget)
		return;

	child      = GTK_IS_CONTAINER (widget) ? GTK_CONTAINER (widget)->focus_child : NULL;
	grandchild = GTK_IS_CONTAINER (child)  ? GTK_CONTAINER (child)->focus_child  : NULL;

	if (child == fs->file_list)
		fs->last_was_flist = TRUE;
	else if (grandchild != fs->action_button)
		fs->last_was_flist = FALSE;

	d(gtk_widget_path (widget, NULL, &s, NULL));

	d(g_print ("last_was_flist = %d (%s)\n", fs->last_was_flist, s));
	d(g_free (s));

	update_action_enabled (fs);
}

static void
file_selector_realize (GtkWidget *w, FileSelector *fs)
{
	file_selector_request_uri (fs,
				   fs->default_dir && strlen (fs->default_dir)
				   ? fs->default_dir
				   : g_get_home_dir (),
				   GNOME_FileSelector_List_RequestAbsolute);
}

static void
connect_ui_signals (FileSelector *fs)
{
	gtk_signal_connect (GTK_OBJECT (fs->up), "clicked",
			    GTK_SIGNAL_FUNC (on_up_clicked), fs);
	gtk_signal_connect (GTK_OBJECT (fs->shortcut_bar), "url_selected",
			    GTK_SIGNAL_FUNC (on_file_selector_sbar_url_selected), fs);

	gtk_signal_connect (GTK_OBJECT (fs->action_button), "clicked",
			    GTK_SIGNAL_FUNC (on_action_clicked), fs);
	gtk_signal_connect (GTK_OBJECT (fs->cancel_button), "clicked",
			    GTK_SIGNAL_FUNC (on_cancel_clicked), fs);

	gtk_signal_connect (GTK_OBJECT (fs->dialog), "set_focus_child",
			    GTK_SIGNAL_FUNC (on_focus_child), fs);
	gtk_signal_connect (GTK_OBJECT (fs->dialog), "realize",
			    GTK_SIGNAL_FUNC (file_selector_realize),
			    fs);
}

static void
file_selector_create_ui (FileSelector *fs)
{
	GladeXML *ui;
	BonoboObjectClient *object_client;

	ui = glade_xml_new (GLADEDIR "/selector-ui.glade", "toplevel");

#define W(v, n) (fs->##v = glade_xml_get_widget (ui, (n)))
	W (dialog, "toplevel");
	W (path_tree, "tree-combo");
	W (back, "button-back");
	W (up, "button-up");
	W (mkdir, "button-mkdir");
	W (shortcut_bar, "iconbar");
	W (file_list, "flist");
	W (type_combo, "type-combo");
	W (action_button, "action-button");
	W (cancel_button, "cancel-button");
	W (action_label, "action-label");
	W (name_entry, "fentry");
#undef W

	/* we need this to be able to get the combo, and we can't do
	 * it before since we don't have the widget that we already
	 * created */
	object_client = bonobo_widget_get_server (BONOBO_WIDGET (fs->file_list));
	fs->flist = bonobo_object_client_query_interface (
		object_client, "IDL:GNOME/FileSelector/List:1.0", NULL);

	create_view_combo (fs->flist, ui);
	fixup_type_combo  (fs);

	connect_ui_signals (fs);

	update_action_enabled (fs);

	gtk_widget_show (fs->dialog);
	gtk_object_unref (GTK_OBJECT (ui));
}

#define ADD_PROP(prop, prop_type, flags) \
bonobo_property_bag_add (pb, prop_name[prop], prop, prop_type, NULL, _(prop_desc[prop]), flags)

#define RW BONOBO_PROPERTY_READABLE | BONOBO_PROPERTY_WRITEABLE
#define RO BONOBO_PROPERTY_READABLE


static void
add_properties (FileSelector *fs, BonoboControl *control)
{
	BonoboPropertyBag *pb;

	pb = bonobo_property_bag_new (fs_get_prop, fs_set_prop, fs);

	ADD_PROP (PROP_ACCEPT_DIRS,         BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_APPLICATION,         BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_DEFAULT_FILE_NAME,   BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_DEFAULT_LOCATION,    BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_DEFAULT_VIEW,        BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_ENABLE_VFS,          BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_MIME_TYPES,          BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_MIME_TYPES_SEQUENCE, TC_CORBA_sequence_CORBA_string, RW);
	ADD_PROP (PROP_MULTIPLE_SELECTION,  BONOBO_ARG_BOOLEAN, RW);
	ADD_PROP (PROP_REQUESTED_URI,       BONOBO_ARG_STRING,  RW);
	ADD_PROP (PROP_SAVE_MODE,           BONOBO_ARG_BOOLEAN, RW);

	fs->properties = pb;
	bonobo_control_set_properties (control, pb);
}

static void
add_cf_listener (GtkWidget *bw,
		 Bonobo_Unknown listener, 
		 EventKit *kit,
		 CORBA_Environment *ev)
{
	Bonobo_PropertyBag  pb;
	BonoboControlFrame *cf;

	cf = bonobo_widget_get_control_frame (BONOBO_WIDGET (bw));
	pb      = bonobo_control_frame_get_control_property_bag (cf, ev);
	kit->es = Bonobo_PropertyBag_getEventSource (pb, ev);
	kit->id = Bonobo_EventSource_addListener (kit->es, listener, ev);

	/* FIXME: why is this NULL? */
	bonobo_object_release_unref (pb, NULL);

	return;
}

static void
create_listeners (FileSelector *fs)
{
	Bonobo_Unknown listener;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	/* flist listener */
	fs->flist_listener = bonobo_listener_new (
		(BonoboListenerCallbackFn)flist_listener_cb, fs);
	listener = BONOBO_OBJREF (fs->flist_listener);

	/* flist property changes */
	add_cf_listener (fs->file_list, listener, &fs->flist_kit[0], &ev);

	/* flist events */
	fs->flist_kit[1].es =
		Bonobo_Unknown_queryInterface (fs->flist, 
					       "IDL:Bonobo/EventSource:1.0", 
					       &ev);
	fs->flist_kit[1].id = 
		Bonobo_EventSource_addListener (fs->flist_kit[1].es, 
						listener, &ev);

	/* entry listener */
	fs->entry_listener = bonobo_listener_new (
		(BonoboListenerCallbackFn)entry_listener_cb, fs);
	listener = BONOBO_OBJREF (fs->entry_listener);

	/* entry property changes */
	add_cf_listener (fs->name_entry, listener, &fs->entry_kit, &ev);	
	
	/* we actually don't need this as the entry talks to the
	 * flist */
#if 0
	es = Bonobo_Unknown_queryInterface (control, "IDL:Bonobo/EventSource:1.0", &ev);
	Bonobo_EventSource_addListener (es, listener, &ev);
	bonobo_object_release_unref (es, NULL);
#endif

	CORBA_exception_free (&ev);
}
static void
load_uri_lists (FileSelector *fs)
{
	char *s;
	/* load favorites / recent things here */

	fs->home_uri = gnome_vfs_uri_new (g_get_home_dir ());

	s = gnome_util_prepend_user_home (".gnome-desktop");
	fs->desktop_uri = gnome_vfs_uri_new (s);
	g_free (s);

	s = gnome_util_prepend_user_home ("Documents");
	fs->docs_uri = gnome_vfs_uri_new (s);
	g_free (s);
}

static void
free_kit (EventKit *kit, CORBA_Environment *ev)
{
	Bonobo_EventSource_removeListener (kit->es, kit->id, ev);

	bonobo_object_release_unref (kit->es, ev);
}

static void
file_selector_destroy (GtkObject *object, gpointer data)
{
	CORBA_Environment ev;
	FileSelector *fs;

	fs = data;

	g_print ("file_selector_destroy ()\n");

	CORBA_exception_init (&ev);

	bonobo_object_unref (BONOBO_OBJECT (fs->properties));

	free_kit (&fs->flist_kit[0], &ev);
	free_kit (&fs->flist_kit[1], &ev);
	free_kit (&fs->entry_kit, &ev);

	bonobo_object_unref (BONOBO_OBJECT (fs->flist_listener));
	bonobo_object_unref (BONOBO_OBJECT (fs->entry_listener));
	
	if (fs->current_uri)
		gnome_vfs_uri_unref (fs->current_uri);

	if (fs->requested_uri)
		gnome_vfs_uri_unref (fs->requested_uri);

	if (fs->home_uri)
		gnome_vfs_uri_unref (fs->home_uri);
	
	if (fs->desktop_uri)
		gnome_vfs_uri_unref (fs->desktop_uri);

	if (fs->docs_uri)
		gnome_vfs_uri_unref (fs->docs_uri);	
	
	bonobo_object_release_unref (fs->flist, &ev);

	if (fs->parent_uris)
		free_uri_list (&fs->parent_uris);

	if (fs->favorite_uris)
		free_uri_list (&fs->favorite_uris);

	if (fs->recent_uris)
		free_uri_list (&fs->recent_uris);

	if (fs->mime_names)
		g_strfreev (fs->mime_names);
	g_free (fs->mime_types);

	g_free (fs->app_name);
	g_free (fs->default_name);
	g_free (fs->default_view);

	CORBA_exception_free (&ev);

	memset (fs, 0, sizeof (FileSelector));
	g_free (fs);

	g_print ("\n [ file selector destroyed ] \n\n");
}

BonoboObject *
file_selector_control_new (void)
{
	BonoboControl *control;
	FileSelector  *fs;

	fs = g_new0 (FileSelector, 1);

	glade_gnome_init ();

	file_selector_create_ui (fs);

	load_uri_lists (fs);

	control = bonobo_control_new (fs->dialog);

	fs->event_source = bonobo_event_source_new ();
	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (fs->event_source));

	add_properties (fs, control);

	property_bag_item_handler_new (BONOBO_OBJECT (control), fs->properties);

	create_listeners (fs);

	gtk_signal_connect (GTK_OBJECT (control), "destroy",
			    GTK_SIGNAL_FUNC (file_selector_destroy),
			    fs);

#if 0
	/* have this here for now... */
	file_selector_request_uri (fs, g_get_home_dir (), GNOME_FileSelector_List_RequestAbsolute);
#endif

	return BONOBO_OBJECT (control);
}
