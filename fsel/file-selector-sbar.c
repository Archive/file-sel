/*
 * Author:
 *  Lutz M�ller <urc8@rz.uni-karlsruhe.de>
 */

#include "file-selector-sbar.h"

#include <gtk/gtkselection.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkdnd.h>
#include <gal/util/e-util.h>
#include <gal/widgets/e-gui-utils.h>
#include <gal/widgets/e-popup-menu.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>
#include <libgnome/gnome-mime.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <string.h>

#include "file-selector-model.h"

#define PARENT_TYPE E_TYPE_SHORTCUT_BAR
static EShortcutBarClass *parent_class;

enum {
	TARGET_SHORTCUT,
	TARGET_URI_LIST
};

enum
{
	URL_SELECTED,
	LAST_SIGNAL
};
static guint file_selector_sbar_signals[LAST_SIGNAL] = {0};

/*
 * There is a bug in the e-shortcut-bar (they don't check for
 * (info == TARGET_SHORTCUT) there). I'd like to fix it, but
 * I don't know whom to ask for permission.
 * For now, we need to do this bad, ugly hack: If dropping an item, 
 * delete the item that got inserted before.
 */
#define UGLY_HACK

struct _FileSelectorSBarPrivate
{
};

static void
drag_data_received_cb (GtkWidget *widget, GdkDragContext *context,
		       gint x, gint y, GtkSelectionData *selection_data,
		       guint info, guint time, gpointer data)
{
	FileSelectorSBar *sbar;
	FileSelectorModel *model;
	EIconBar *icon_bar;
	GList *uris, *uri;
	int item_num, group_num;

	sbar = FILE_SELECTOR_SBAR (data);
	model = FILE_SELECTOR_MODEL (E_SHORTCUT_BAR (sbar)->model);
	icon_bar = E_ICON_BAR (widget);
	item_num = icon_bar->dragging_before_item_num;
	group_num = e_group_bar_get_group_num (E_GROUP_BAR (sbar),
					       GTK_WIDGET (icon_bar)->parent);

	if ((selection_data->length >= 0) && (selection_data->format == 8)
	    && (info == TARGET_URI_LIST)) {
#ifdef UGLY_HACK
		e_shortcut_model_remove_item (E_SHORTCUT_MODEL (model),
					      group_num, item_num);
#endif
		uris = gnome_uri_list_extract_uris (selection_data->data);
		for (uri = uris; uri; uri = uri->next) {
			file_selector_model_add_item (model,
					group_num, item_num++,
					uri->data, uri->data,
					IMAGEDIR "/../gnome-logo-icon.png");
		}
		gtk_drag_finish (context, TRUE, TRUE, time);
	} else
		gtk_drag_finish (context, FALSE, FALSE, time);
}

static void
setup_drag_and_drop (FileSelectorSBar *sbar)
{
	EShortcutBarGroup *group;
	static const GtkTargetEntry targets [] = {
		{ "text/uri-list", 0, TARGET_URI_LIST },
		{ "E-SHORTCUT",    0, TARGET_SHORTCUT }
	}; 
	int i;

	for (i = 0; i < E_SHORTCUT_BAR (sbar)->groups->len; i++) {
		group = &g_array_index (E_SHORTCUT_BAR (sbar)->groups,
				        EShortcutBarGroup, i);
		gtk_drag_dest_set (group->icon_bar,
				GTK_DEST_DEFAULT_MOTION |
				GTK_DEST_DEFAULT_DROP, targets,
				sizeof (targets) / sizeof (targets[0]),
				GDK_ACTION_COPY);
		gtk_signal_connect (GTK_OBJECT (group->icon_bar), 
				    "drag_data_received",
				    GTK_SIGNAL_FUNC (drag_data_received_cb),
				    sbar);
	}
}

static void
file_selector_sbar_destroy (GtkObject *object)
{
	FileSelectorSBar *sbar;

	sbar = FILE_SELECTOR_SBAR (object);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
file_selector_sbar_finalize (GtkObject *object)
{
	FileSelectorSBar *sbar;

	sbar = FILE_SELECTOR_SBAR (object);
	
	g_free (sbar->priv);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

typedef struct {
	EShortcutModel *model;
	int group_num;
} NewGroupData;

static void
new_group_cb (gchar *string, gpointer data)
{
	NewGroupData *ngd = data;

	if (string) 
		e_shortcut_model_add_group (ngd->model, ngd->group_num, string);
	g_free (ngd);
}

static void
on_add_group_clicked (GtkButton *button, gpointer data)
{
	GtkWidget *widget;
	GtkWindow *window = NULL;
	GtkWidget *dialog;
	NewGroupData *ngd;

	ngd = g_new (NewGroupData, 1);
	ngd->group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT(data),
			                                       "group_num"));
	ngd->model = E_SHORTCUT_BAR (data)->model;

	widget = gtk_widget_get_ancestor (GTK_WIDGET (data), GTK_TYPE_WINDOW);
	if (widget)
		window = GTK_WINDOW (widget);
	dialog = gnome_request_dialog (FALSE, _("Please enter a name for the "
				       "new group"), _("New Group"), 100, 
				       new_group_cb, ngd, window);
}

static void
on_delete_group_clicked (GtkButton *button, gpointer data)
{
	GtkWidget *window;
	EShortcutBar *shortcut_bar;
	int group_num;
	char *message = _("You cannot remove the last group!");
	
	g_return_if_fail (E_IS_SHORTCUT_BAR (data));
	shortcut_bar = E_SHORTCUT_BAR (data);

	if (e_shortcut_model_get_num_groups (shortcut_bar->model) == 1) {
		window = gtk_widget_get_ancestor (GTK_WIDGET (shortcut_bar),
						  GTK_TYPE_WINDOW);
		if (window)
			gnome_error_dialog_parented (message, 
						     GTK_WINDOW (window));
		else
			gnome_error_dialog (message);
		return;
	}
	
        group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
				     "group_num"));
	e_shortcut_model_remove_group (shortcut_bar->model, group_num);
}

static void
on_add_item_clicked (GtkButton *button, gpointer data)
{
	FileSelectorSBar *sbar;
	FileSelectorModel *model; 
	GtkWidget *widget;
	GtkWindow *window = NULL;
	int item_num, group_num;

	g_return_if_fail (FILE_SELECTOR_IS_SBAR (data));
	sbar = FILE_SELECTOR_SBAR (data);
	model = FILE_SELECTOR_MODEL (E_SHORTCUT_BAR (sbar)->model); 

	widget = gtk_widget_get_ancestor (GTK_WIDGET (data), GTK_TYPE_WINDOW);
	if (widget)
		window = GTK_WINDOW (widget);
	item_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							 "item_num"));
	group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data), 
							  "group_num"));
	file_selector_model_add_item_ask (model, group_num, item_num, window);
}

static void
on_edit_item_clicked (GtkButton *button, gpointer data)
{
	FileSelectorSBar *sbar;
	FileSelectorModel *model;
	GtkWidget *widget;
	GtkWindow *window = NULL;
	int item_num, group_num;

	g_return_if_fail (FILE_SELECTOR_IS_SBAR (data));
	sbar = FILE_SELECTOR_SBAR (data);
	model = FILE_SELECTOR_MODEL (E_SHORTCUT_BAR (sbar)->model);

	widget = gtk_widget_get_ancestor (GTK_WIDGET (data), GTK_TYPE_WINDOW);
	if (widget)
		window = GTK_WINDOW (widget);
	item_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							 "item_num"));
	group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							  "group_num"));
	file_selector_model_update_item_ask (model, group_num, item_num,
					     window);
}

static void
on_rename_item_clicked (GtkButton *button, gpointer data)
{
	int item_num, group_num;

	item_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data), 
							 "item_num"));
	group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							  "group_num"));
	e_shortcut_bar_start_editing_item (E_SHORTCUT_BAR (data), group_num, 
					   item_num);
}

static void
on_delete_item_clicked (GtkButton *button, gpointer data)
{
	EShortcutBar *shortcut_bar;
	int item_num, group_num;

	g_return_if_fail (E_IS_SHORTCUT_BAR (data));
	shortcut_bar = E_SHORTCUT_BAR (data);

	item_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							 "item_num"));
	group_num = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (data),
							  "group_num"));
	e_shortcut_model_remove_item (shortcut_bar->model, group_num, item_num);
}

static EPopupMenu group_menu [] = {
	{ N_("Add item"), NULL, GTK_SIGNAL_FUNC (on_add_item_clicked), NULL, 0},
	E_POPUP_SEPARATOR,
	{ N_("Add group"), NULL, GTK_SIGNAL_FUNC (on_add_group_clicked),NULL,0},
	{ N_("Delete group"), NULL, GTK_SIGNAL_FUNC (on_delete_group_clicked),
	  NULL, 0},
	E_POPUP_TERMINATOR
};

static EPopupMenu item_menu [] = {
	{ N_("Add item"), NULL, GTK_SIGNAL_FUNC (on_add_item_clicked), NULL, 0},
	{ N_("Edit item"), NULL, GTK_SIGNAL_FUNC (on_edit_item_clicked),NULL,0},
	{ N_("Rename item"), NULL, GTK_SIGNAL_FUNC (on_rename_item_clicked), 
	  NULL, 0},
	{ N_("Delete item"), NULL, GTK_SIGNAL_FUNC (on_delete_item_clicked),
	  NULL, 0},
	E_POPUP_SEPARATOR,
	{ N_("Add group"), NULL, GTK_SIGNAL_FUNC (on_add_group_clicked),NULL,0},
	{ N_("Delete group"), NULL, GTK_SIGNAL_FUNC (on_delete_group_clicked),
	  NULL, 0},
	E_POPUP_TERMINATOR
};

static void
file_selector_sbar_item_selected (EShortcutBar *shortcut_bar, GdkEvent *event,
			          gint group_num, gint item_num)
{
	char *url;
	GtkMenu *menu;

	if ((event->button.button == 1) && (item_num >= 0)) {
		e_shortcut_model_get_item_info (shortcut_bar->model,
						group_num, item_num, &url,
						NULL, NULL);
		gtk_signal_emit (GTK_OBJECT (shortcut_bar), 
				 file_selector_sbar_signals [URL_SELECTED],
				 url);
	} else if (event->button.button == 3) {
		gtk_object_set_data (GTK_OBJECT (shortcut_bar), "item_num",
				     GINT_TO_POINTER (item_num));
		gtk_object_set_data (GTK_OBJECT (shortcut_bar), "group_num",
				     GINT_TO_POINTER (group_num));
		if (item_num < 0)
			menu = e_popup_menu_create (group_menu, 0, 0,
						    shortcut_bar);
		else
			menu = e_popup_menu_create (item_menu, 0, 0, 
						    shortcut_bar);
		e_auto_kill_popup_menu_on_hide (menu);
		gtk_menu_popup (menu, NULL, NULL, NULL, NULL,
				event->button.button, event->button.time);
	}
}

static void
file_selector_sbar_shortcut_dropped (EShortcutBar *shortcut_bar,
				     gint group_num, gint item_num,
				     const gchar *url,
				     const gchar *name)
{
	FileSelectorSBar *sbar;

	g_return_if_fail (FILE_SELECTOR_IS_SBAR (shortcut_bar));
	sbar = FILE_SELECTOR_SBAR (shortcut_bar);

	file_selector_model_add_item (FILE_SELECTOR_MODEL (shortcut_bar->model),
				      group_num, item_num, url, name, 
				      IMAGEDIR "/../gnome-logo-icon.png");
}

static void
file_selector_sbar_shortcut_dragged (EShortcutBar *shortcut_bar,
				     gint group_num, gint item_num)
{
	e_shortcut_model_remove_item (shortcut_bar->model,
				      group_num, item_num);
}

static void
file_selector_sbar_class_init (FileSelectorSBarClass *klass)
{
	GtkObjectClass *object_class;
	EShortcutBarClass *shortcut_bar_class;

	parent_class = gtk_type_class (PARENT_TYPE);

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->destroy  = file_selector_sbar_destroy;
	object_class->finalize = file_selector_sbar_finalize;

	shortcut_bar_class = E_SHORTCUT_BAR_CLASS (klass);
	shortcut_bar_class->item_selected    = file_selector_sbar_item_selected;
	shortcut_bar_class->shortcut_dragged = file_selector_sbar_shortcut_dragged;
	shortcut_bar_class->shortcut_dropped = file_selector_sbar_shortcut_dropped;

	file_selector_sbar_signals[URL_SELECTED] =
		gtk_signal_new ("url_selected", GTK_RUN_LAST | GTK_RUN_ACTION,
				object_class->type,
				GTK_SIGNAL_OFFSET (FileSelectorSBarClass,
						   url_selected),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
	gtk_object_class_add_signals (object_class, file_selector_sbar_signals,
				      LAST_SIGNAL);
}

static void
file_selector_sbar_init (FileSelectorSBar *sbar)
{
	sbar->priv = g_new0 (FileSelectorSBarPrivate, 1);
}

E_MAKE_TYPE (file_selector_sbar, "FileSelectorSBar", FileSelectorSBar, file_selector_sbar_class_init, file_selector_sbar_init, PARENT_TYPE)

GtkWidget *
file_selector_sbar_new (void)
{
	FileSelectorSBar *sbar;
	EShortcutModel *model;

	sbar = gtk_type_new (FILE_SELECTOR_TYPE_SBAR);

	model = file_selector_model_new ();
	e_shortcut_bar_set_model (E_SHORTCUT_BAR (sbar), model);

	setup_drag_and_drop (sbar);

	return (GTK_WIDGET (sbar));
}
