/*
 * file-selector.h: implementation of GNOME::FileSelector
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *    Miguel de Icaza  <miguel@ximian.com>
 *
 * Copyright 2000, 2001 Ximian, Inc.
 */

#ifndef _FILE_SELECTOR_H_
#define _FILE_SELECTOR_H_

#include <bonobo/bonobo-object.h>

BonoboObject *file_selector_control_new (void);

#endif

