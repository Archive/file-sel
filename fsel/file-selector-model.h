/*
 * Author:
 *  Lutz M�ller <urc8@rz.uni-karlsruhe.de>
 */

#ifndef _FILE_SELECTOR_MODEL_H_
#define _FILE_SELECTOR_MODEL_H_

#include <gal/shortcut-bar/e-shortcut-model.h>
#include <libgnome/gnome-defs.h>
#include <gtk/gtkwindow.h>

BEGIN_GNOME_DECLS

#define FILE_SELECTOR_TYPE_MODEL         (file_selector_model_get_type ())
#define FILE_SELECTOR_MODEL(obj)         (GTK_CHECK_CAST (obj, FILE_SELECTOR_TYPE_MODEL, FileSelectorModel))
#define FILE_SELECTOR_MODEL_CLASS(klass) (GTK_CHECK_CLASS_CAST (klass, FILE_SELECTOR_TYPE_MODEL, FileSelectorModelClass))
#define FILE_SELECTOR_IS_MODEL(obj)      (GTK_CHECK_TYPE (obj, FILE_SELECTOR_TYPE_MODEL))

typedef struct _FileSelectorModel        FileSelectorModel;
typedef struct _FileSelectorModelPrivate FileSelectorModelPrivate;
typedef struct _FileSelectorModelClass   FileSelectorModelClass;

struct _FileSelectorModel
{
	EShortcutModel parent;

	FileSelectorModelPrivate *priv;
};

struct _FileSelectorModelClass
{
	EShortcutModelClass parent_class;
};

GtkType         file_selector_model_get_type (void);
EShortcutModel *file_selector_model_new      (void);

void	file_selector_model_add_item_ask    (FileSelectorModel *model,
				             int group_num, int item_num,
				             GtkWindow *parent);
void	file_selector_model_update_item_ask (FileSelectorModel *model,
					     int group_num, int item_num,
					     GtkWindow *parent);

int     file_selector_model_add_item        (FileSelectorModel *model, 
					     int group_num, int item_num,
					     const char *url, 
					     const char *name,
					     const char *image);
void	file_selector_model_update_item     (FileSelectorModel *model,
					     int group_num, int item_num, 
					     const char *url,
					     const char *name,
					     const char *image);

END_GNOME_DECLS

#endif /* _FILE_SELECTOR_MODEL_H_ */
