/*
 * Author:
 *  Lutz M�ller <urc8@rz.uni-karlsruhe.de>
 */

#include "file-selector-model.h"

#include <gtk/gtkselection.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkdnd.h>
#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtkentry.h>
#include <gtk/gtklabel.h>
#include <gal/util/e-util.h>
#include <gal/widgets/e-gui-utils.h>
#include <gal/widgets/e-popup-menu.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>
#include <libgnome/gnome-mime.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-icon-entry.h>
#include <libgnomeui/gnome-file-entry.h>
#include <libgnomeui/gnome-stock.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <string.h>

#define PARENT_TYPE E_TYPE_SHORTCUT_MODEL
static EShortcutModelClass *parent_class;

typedef struct
{
	char *name;
	char *url;
	char *image;
} DefaultItem;

struct _FileSelectorModelPrivate
{
	gchar *filename;

	GHashTable *images;

	DefaultItem default_item [4];
};

static void
file_selector_model_load_default (FileSelectorModel *fsel_model)
{
	EShortcutModel *model;
	int group_num, i;

	g_return_if_fail (E_IS_SHORTCUT_MODEL (fsel_model));
	model = E_SHORTCUT_MODEL (fsel_model);

	group_num = e_shortcut_model_add_group (model, -1, _("My Shortcuts"));

	for (i = 0; i < 4; i++)
		file_selector_model_add_item (fsel_model, group_num, -1, 
				fsel_model->priv->default_item[i].url,
				fsel_model->priv->default_item[i].name,
				fsel_model->priv->default_item[i].image);
}

static void
file_selector_model_save (FileSelectorModel *fsel_model)
{
	EShortcutModel *model;
	xmlDoc *doc;
	xmlNode *root, *group, *item;
	char *title;
	char *url, *name, *image;
	int num_groups, num_items;
	int i, j;
	GdkPixbuf *pixbuf;

	g_return_if_fail (E_IS_SHORTCUT_MODEL (fsel_model));
	model = E_SHORTCUT_MODEL (fsel_model);

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "shortcuts", NULL);
	xmlDocSetRootElement (doc, root);

	num_groups = e_shortcut_model_get_num_groups (model);
	for (i = 0; i < num_groups; i++) {
		title = e_shortcut_model_get_group_name (model, i);
		group = xmlNewChild (root, NULL, "group", NULL);
		g_assert (group);
		xmlSetProp (group, "title", title);
		g_free (title);
		num_items = e_shortcut_model_get_num_items (model, i);
		for (j = 0; j < num_items; j++) {
			item = xmlNewChild (group, NULL, "item", "");
			e_shortcut_model_get_item_info (model, i, j,
							&url, &name, &pixbuf);
			xmlSetProp (item, "name", name);
			xmlSetProp (item, "url", url);
			g_free (name);
			g_free (url);
			image = g_hash_table_lookup (fsel_model->priv->images, 
						     &pixbuf);
			gdk_pixbuf_unref (pixbuf);
			if (!image) {
				g_warning ("Could not find the file that "
					   "corresponds to a pixbuf. Somebody "
					   "probably forgot to "
					   "g_hash_table_insert the path into "
					   "model->priv->images.");
				xmlSetProp (item, "image",
					    IMAGEDIR "/../gnome-logo-icon.png");
			} else
				xmlSetProp (item, "image", image);
		}
	}

	if (xmlSaveFile (fsel_model->priv->filename, doc) < 0)
		g_warning ("Could not save to '%s'.",
			   fsel_model->priv->filename);

	xmlFreeDoc (doc);
}

static void
file_selector_model_load (FileSelectorModel *fsel_model)
{
	EShortcutModel *model;
	xmlDoc *doc;
	xmlNode *root, *group, *item;
	gchar *title, *name, *url, *image;
	int group_num;

	g_return_if_fail (E_IS_SHORTCUT_MODEL (fsel_model));
	model = E_SHORTCUT_MODEL (fsel_model);
	
	doc = xmlParseFile (fsel_model->priv->filename);
	if (!doc) {
		g_warning ("Could not read '%s'. I am going to show "
			   "default items.", fsel_model->priv->filename);
		file_selector_model_load_default (fsel_model);
		return;
	}

	root = xmlDocGetRootElement (doc);
	if (!root) {
		g_warning ("Could not get root element!");
		xmlFreeDoc (doc);
		return;
	}

	for (group = root->childs; group != NULL; group = group->next) {
		title = xmlGetProp (group, "title");
		group_num = e_shortcut_model_add_group (model, -1, title);
		xmlFree (title);
		for (item = group->childs; item != NULL; item = item->next) {
			name = xmlGetProp (item, "name");
			url = xmlGetProp (item, "url");
			image = xmlGetProp (item, "image");
			file_selector_model_add_item (fsel_model, group_num,
						      -1, url, name, image);
			xmlFree (name);
			xmlFree (url);
			xmlFree (image);
		}
	}

	xmlFreeDoc (doc);
}

static void
foreach_func (gpointer key, gpointer value, gpointer data)
{
	g_free (value);
	value = NULL;
}

static void
file_selector_model_destroy (GtkObject *object)
{
	FileSelectorModel *model;
	int i;

	model = FILE_SELECTOR_MODEL (object);

	g_free (model->priv->filename);
	model->priv->filename = NULL;

	g_hash_table_foreach (model->priv->images, foreach_func, NULL);
	g_hash_table_destroy (model->priv->images);
	model->priv->images = NULL;

	for (i = 0; i < 4; i++) {
		g_free (model->priv->default_item[i].url);
		model->priv->default_item[i].url = NULL;
		g_free (model->priv->default_item[i].name);
		model->priv->default_item[i].name = NULL;
		g_free (model->priv->default_item[i].image);
		model->priv->default_item[i].image = NULL;
	}
	
	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
file_selector_model_finalize (GtkObject *object)
{
	FileSelectorModel *model;

	model = FILE_SELECTOR_MODEL (object);
	
	g_free (model->priv);

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
file_selector_model_class_init (FileSelectorModelClass *klass)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class (PARENT_TYPE);

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->destroy  = file_selector_model_destroy;
	object_class->finalize = file_selector_model_finalize;
}

static void
file_selector_model_init (FileSelectorModel *model)
{
	FileSelectorModelPrivate *priv;

	model->priv = g_new0 (FileSelectorModelPrivate, 1);
	priv = model->priv;

	model->priv->images = g_hash_table_new (g_int_hash, g_int_equal);

	priv->filename = gnome_util_prepend_user_home (".GNOME_FileSel.xml");

	priv->default_item[0].url = gnome_util_prepend_user_home (".gnome-desktop");
	priv->default_item[0].name = g_strdup (_("Desktop"));
	priv->default_item[0].image = g_strdup (IMAGEDIR "/my_desktop.png");

	priv->default_item[1].url = gnome_util_prepend_user_home ("Documents"); 
	priv->default_item[1].name = g_strdup (_("Documents"));
	priv->default_item[1].image = g_strdup (IMAGEDIR "/my_documents.png");

	priv->default_item[2].url = g_strdup (g_get_home_dir ());
	priv->default_item[2].name = g_strdup (_("Home"));
	priv->default_item[2].image = g_strdup (IMAGEDIR "/my_home.png");

	priv->default_item[3].url = g_strdup ("/");
	priv->default_item[3].name = g_strdup (_("Computer"));
	priv->default_item[3].image = g_strdup (IMAGEDIR "/my_computer_alt.png");
}

E_MAKE_TYPE (file_selector_model, "FileSelectorModel", FileSelectorModel, file_selector_model_class_init, file_selector_model_init, PARENT_TYPE)

static void
group_added_cb (EShortcutModel *model, int group_num, const gchar *group_name,
		gpointer data)
{
	file_selector_model_save (FILE_SELECTOR_MODEL (model));
}

static void
group_removed_cb (EShortcutModel *model, int group_num, gpointer data)
{
	file_selector_model_save (FILE_SELECTOR_MODEL (model));
}

static void
item_added_cb (EShortcutModel *model, int group_num, int item_num, 
	       const char *item_url, const char *item_name, GdkPixbuf *pixbuf, 
	       gpointer data)
{
	FileSelectorModel *fsel_model;

	g_return_if_fail (FILE_SELECTOR_IS_MODEL (model));
	fsel_model = FILE_SELECTOR_MODEL (model);

	file_selector_model_save (fsel_model);
}

static void
item_removed_cb (EShortcutModel *model, int group_num, int item_num, 
		 gpointer data)
{
	file_selector_model_save (FILE_SELECTOR_MODEL (model));
}

static void
item_updated_cb (EShortcutModel *model, int group_num, int item_num,
		 const char *item_url, const char *item_name, GdkPixbuf *pixbuf,
		 gpointer data)
{
	file_selector_model_save (FILE_SELECTOR_MODEL (model));
}

EShortcutModel *
file_selector_model_new (void)
{
	FileSelectorModel *model;

	model = gtk_type_new (FILE_SELECTOR_TYPE_MODEL);

	file_selector_model_load (model);

	gtk_signal_connect (GTK_OBJECT (model), "group_added", 
			    GTK_SIGNAL_FUNC (group_added_cb), model);
	gtk_signal_connect (GTK_OBJECT (model), "group_removed",
			    GTK_SIGNAL_FUNC (group_removed_cb), model);
	gtk_signal_connect (GTK_OBJECT (model), "item_added",
			    GTK_SIGNAL_FUNC (item_added_cb), model);
	gtk_signal_connect (GTK_OBJECT (model), "item_removed",
			    GTK_SIGNAL_FUNC (item_removed_cb), model);
	gtk_signal_connect (GTK_OBJECT (model), "item_updated",
			    GTK_SIGNAL_FUNC (item_updated_cb), model);

	return (E_SHORTCUT_MODEL (model));
}

typedef struct
{
	FileSelectorModel *model;
	gboolean new;
	GtkWidget *entry_name;
	GtkWidget *entry_url;
	GtkWidget *entry_image;
	int group_num;
	int item_num;
} ItemData;

static void
item_dialog_close_cb (GnomeDialog *dialog, ItemData *data)
{
	g_free (data);
}

static void
item_dialog_clicked_cb (GnomeDialog *dialog, int button_number, ItemData *data)
{
	GtkWidget *entry;
	char *url, *name, *image;

	if (button_number == 0) {
		image = gnome_icon_entry_get_filename (GNOME_ICON_ENTRY (data->entry_image));
		if (!image)
			image = g_strdup (IMAGEDIR "/../gnome-logo-icon.png");
		entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (data->entry_url));
		url = gtk_entry_get_text (GTK_ENTRY (entry));
		name = gtk_entry_get_text (GTK_ENTRY (data->entry_name));
		if (data->new)
			file_selector_model_add_item (data->model,
						      data->group_num, 
						      data->item_num, 
						      url, name, image);
		else
			file_selector_model_update_item (data->model, 
							 data->group_num, 
							 data->item_num, 
							 url, name, image);
		g_free (image);
	}
}

static void
show_item_dialog (FileSelectorModel *model, const char *url,
		  const char *name, const char *image, 
		  int group_num, int item_num, gboolean new, GtkWindow *parent)
{
	GtkWidget *dialog, *table, *label, *entry;
	ItemData *data;
	const char *buttons [] = { GNOME_STOCK_BUTTON_OK, 
				   GNOME_STOCK_BUTTON_CANCEL, NULL };

	dialog = gnome_dialog_newv (_("New item"), buttons);
	gnome_dialog_set_default (GNOME_DIALOG (dialog), 0);
	gnome_dialog_set_close (GNOME_DIALOG (dialog), TRUE);
	
	if (parent)
		gnome_dialog_set_parent (GNOME_DIALOG (dialog), parent);

	table = gtk_table_new (2, 3, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 10);
	gtk_table_set_col_spacings (GTK_TABLE (table), 10);
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (dialog)->vbox), table);
	label = gtk_label_new (_("Name of item: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
	label = gtk_label_new (_("URL: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

	data = g_new0 (ItemData, 1);
	data->model = model;
	data->new = new;
	data->group_num = group_num;
	data->item_num = item_num;

	data->entry_name = gtk_entry_new ();
	data->entry_url = gnome_file_entry_new (NULL, NULL);
	data->entry_image = gnome_icon_entry_new (NULL, NULL);
	
	gtk_widget_show (data->entry_name);
	gtk_widget_show (data->entry_url);
	gtk_widget_show (data->entry_image);
	
	gtk_table_attach_defaults (GTK_TABLE (table), data->entry_name, 
				   1, 2, 0, 1);
	gtk_table_attach_defaults (GTK_TABLE (table), data->entry_url, 
				   1, 2, 1, 2);
	gtk_table_attach_defaults (GTK_TABLE (table), data->entry_image,
				   2, 3, 0, 2);

	if (name)
		gtk_entry_set_text (GTK_ENTRY (data->entry_name), name);
	if (url) {
		entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (data->entry_url));
		gtk_entry_set_text (GTK_ENTRY (entry), url);
		gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (data->entry_url), url);
	}
	if (image)
		gnome_icon_entry_set_icon (GNOME_ICON_ENTRY (data->entry_image),
					   image);

	gtk_signal_connect (GTK_OBJECT (dialog), "clicked",
			    GTK_SIGNAL_FUNC (item_dialog_clicked_cb), data);
	gtk_signal_connect (GTK_OBJECT (dialog), "close", 
			    GTK_SIGNAL_FUNC (item_dialog_close_cb), data);

	gtk_widget_show (dialog);
}

int
file_selector_model_add_item (FileSelectorModel *model, int group_num,
			      int item_num, const char *url, 
			      const char *name, const char *image)
{
	GdkPixbuf *pixbuf;
	int *key;

	g_return_val_if_fail (FILE_SELECTOR_IS_MODEL (model), -1);

	pixbuf = gdk_pixbuf_new_from_file (image);
	if (!pixbuf) {
		g_warning ("Could not create pixbuf from '%s'.", image);
		return (-1);
	}
	key = g_new (int, 1);
	*key = GPOINTER_TO_INT (pixbuf);
	g_hash_table_insert (model->priv->images, key, g_strdup (image));
	item_num = e_shortcut_model_add_item (E_SHORTCUT_MODEL (model),
					      group_num, item_num, url, name,
					      pixbuf);
	gdk_pixbuf_unref (pixbuf);

	return (item_num);
}

void
file_selector_model_add_item_ask (FileSelectorModel *model, int group_num, 
			      int item_num, GtkWindow *parent)
{
	g_return_if_fail (FILE_SELECTOR_IS_MODEL (model));

	show_item_dialog (model, NULL, NULL, NULL, group_num, item_num, TRUE,
			  parent);
}

void
file_selector_model_update_item_ask (FileSelectorModel *model, int group_num,
			             int item_num, GtkWindow *parent)
{
	char *url, *name, *image;
	GdkPixbuf *pixbuf;

	g_return_if_fail (FILE_SELECTOR_IS_MODEL (model));

	e_shortcut_model_get_item_info (E_SHORTCUT_MODEL (model), group_num,
					item_num, &url, &name, &pixbuf);
	image = g_hash_table_lookup (model->priv->images, &pixbuf);
	if (!image) {
		g_warning ("Could not find the path to the file for a pixbuf!");
		g_free (url);
		g_free (name);
		return;
	}
	show_item_dialog (model, url, name, image, group_num, item_num, FALSE,
			  parent);
	g_free (url);
	g_free (name);
}

void 
file_selector_model_update_item (FileSelectorModel *model, int group_num,
				 int item_num, const char *url,
				 const char *name, const char *image)
{
	GdkPixbuf *pixbuf;
	int *key;
	
	g_return_if_fail (FILE_SELECTOR_IS_MODEL (model));

	pixbuf = gdk_pixbuf_new_from_file (image);
	if (!pixbuf) {
		g_warning ("Could not create pixbuf from '%s'.",                                           image);
		return;
	}
	key = g_new (int, 1);
	*key = GPOINTER_TO_INT (pixbuf);
	g_hash_table_insert (model->priv->images, key, g_strdup (image));
	e_shortcut_model_update_item (E_SHORTCUT_MODEL (model),
				      group_num, item_num, url, name, pixbuf);
	gdk_pixbuf_unref (pixbuf);
}

